/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package db_code;

import Constants.Strings;
import DataStructure.Activity;
import DataStructure.Activity.*;
import DataStructure.Date;
import DataStructure.OverallResult;
import DataStructure.Query;
import DataStructure.Result;
import DataStructure.Tuple_of_ACCount;
import DataStructure.Tuple_of_Ac;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.Scanner;
import ssc.Main;
import oracle.toplink.essentials.*;

/**
 *
 * @author Makan TAyebi
 */
public class MySQLConnector {

    private String USERNAME = "the username here";
    private String PASSWORD = "the password here";
    private String DATABASE = "database name here";
    private static String ROOT_ACCOUNT = "root account here";
    private static String ROOT_PASSWORD = "root password here";

    public MySQLConnector() {
    }

    public void setPassword(String password) {
        PASSWORD = password;
    }
    
    public boolean isPassValid(String pass) {
        Connection con = connectToServer(USERNAME, pass);
        return con != null;
    }
    /* --------------------------------------------------------- */

    private boolean searchForDep(int dep_ID) throws SQLException {
        Connection con = connectToDB(USERNAME, PASSWORD);
        if (con != null) {
            Statement statement = con.createStatement();
            ResultSet res = statement.executeQuery(
                    " SELECT COUNT(dep_ID)"
                    + "FROM activity"
                    + " WHERE dep_ID= " + dep_ID);
            res.next();
            if (res.getInt("COUNT(dep_ID)") == 0) {
                closeConnection(con);
                return false;
            } else {
                closeConnection(con);
                return true;
            }
        }
        return true;
    }

    private boolean searchForAT(int at_ID) throws SQLException {
        Connection con = connectToDB(USERNAME, PASSWORD);
        if (con != null) {
            Statement statement = con.createStatement();
            ResultSet res = statement.executeQuery(
                    " SELECT COUNT(ac_ID)"
                    + "FROM activity"
                    + " WHERE ac_ID= " + at_ID);
            res.next();
            if (res.getInt("COUNT(ac_ID)") == 0) {
                closeConnection(con);
                return false;
            } else {
                closeConnection(con);
                return true;
            }
        }
        return true;
    }

    /**
     *
     * @param dep_ID
     * @param modeBit : if modeBit is true, then it means user forces the delete.
     *  else it means look for the at_ID in activity table, if there is any
     *  of activities with ID: at_ID
     * @return
     */
    public boolean deleteActivityType(int at_ID, boolean modeBit) throws SQLException {
        Connection con = connectToDB(USERNAME, PASSWORD);
        if (con != null) {
            Statement statement = con.createStatement();
            if (!searchForAT(at_ID)) {
                //if at_ID is not used:
                statement.execute(
                        " DELETE FROM activity_type"
                        + " WHERE at_ID=" + at_ID);

                return true; // i deleted the AT.
            } else if (modeBit) {
                //if at_ID is used, and user forces delete:
                statement.execute(
                        " DELETE FROM activity_type"
                        + " WHERE at_ID=" + at_ID);
//                System.out.println("starting to delete aaaaalll  activities related to " + at_ID);
                statement = con.createStatement();
                statement.execute(
                        " DELETE FROM activity"
                        + " WHERE at_ID=" + at_ID);
                closeConnection(con);
            } else {
                //if modebit is false, and at_ID is used.
                return false;
                /* i returned false; because i did not delete the AT,
                 * there was activities related to this at_ID. and as you
                 * said by the modeBit, i did not force delete.
                 */
            }
        }
        return false;
    }

    /**
     *
     * @param dep_ID
     * @param modeBit if modeBit is true, then it means user forces the delete.
     *  else if modeBit is false, it means function looks for the at_ID in
     * activity table, if there is any of activities in department: dep_ID
     * @return returns true if deletes the department, false if there is
     * activities related to this dep_ID
     * @throws SQLException
     */
    public boolean deleteDepartment(int dep_ID, boolean modeBit) throws SQLException {
        Connection con = connectToDB(USERNAME, PASSWORD);
        if (con != null) {
            Statement statement = con.createStatement();
            if (!searchForDep(dep_ID)) {
                //if dep_ID is not used:
                statement.execute(
                        " DELETE FROM department"
                        + " WHERE dep_ID=" + dep_ID);
                closeConnection(con);
                return true; // i deleted the department.
            } else if (modeBit) {
                //if dep_ID is used, and user forces delete:
                statement.execute(
                        " DELETE FROM department"
                        + " WHERE dep_ID=" + dep_ID);
                statement.execute(
                        " DELETE FROM activity"
                        + " WHERE dep_ID=" + dep_ID);
                closeConnection(con);
                return true;
            } else {
                //if modebit is false, and dep_ID is used.
                return false;
                /* i returned false; because i did not delete the department,
                 * there was activities related to this dep_ID. and as you
                 * said by the modeBit, i did not force delete.
                 */
            }
        }
        return false;
    }

    public String getBackGroundPath() {
        Connection con = connectToDB(USERNAME, PASSWORD);
        if (con != null) {
            String query = "select FilePath from WallPaperFilePath where ID=1";
            ResultSet r;
            try {
                Statement statement = con.createStatement();
                r = statement.executeQuery(query);
                r.next();
                String path = r.getString("FilePath");
                closeConnection(con);
                return path;
            } catch (SQLException ex) {
            }
        } else {
        }
        return null;
    }

    public boolean setBackGroundPath(String path) {
        Connection con = connectToDB(USERNAME, PASSWORD);
        if (con == null || "".equals(path)) {
            return false;
        } else {
            String temp = "";
            for (char ch : path.toCharArray()) {
                if (ch == '\\') {
                    temp += "\\\\";
                } else {
                    temp += ch;
                }
            }
            path = temp;
            String q;
            ResultSet r = null;
            try {
                q = "SELECT COUNT(ID) FROM wallpaperfilepath";
                Statement statement = con.createStatement();
                r = statement.executeQuery(q);
                r.next();
                if (r.getInt("COUNT(ID)") == 0) {
                    q =
                            "INSERT INTO "
                            + "wallpaperfilepath (filepath,ID) "
                            + "VALUES (" + "\"" + path + "\"" + ",1)";
                } else {
                    q = "UPDATE wallpaperfilepath  SET filePath=" + "\"" + path + "\"" + " WHERE ID=1";
                }
            } catch (Exception ex) {
                return false;
            }
            try {
                Statement statement = con.createStatement();
                statement.execute(q);
                closeConnection(con);
                return true;
            } catch (SQLException ex) {
                ex.printStackTrace();
                closeConnection(con);
                return false;
            }
        }
    }
    /* --------------------------------------------------------- */

    public boolean addDepartment(String dep_Name) {
        Connection con = connectToDB(USERNAME, PASSWORD);
        if (con != null) {
            try {
                Statement statement = con.createStatement();
                String q =
                        "INSERT "
                        + "INTO "
                        + "department "
                        + "(dep_ID,dep_Name) "
                        + "VALUES (NULL,'" + dep_Name + "')";
                statement.execute(q);
                closeConnection(con);
                return true;
            } catch (SQLException ex) {
                ex.printStackTrace();
                closeConnection(con);
            }
        }
        return false;
    }

    public boolean addActivityType(String at_Name, double ratio) {
        Connection con = connectToDB(USERNAME, PASSWORD);
        if (con != null) {
            try {
                Statement statement = con.createStatement();
                String q =
                        "INSERT INTO activity_type "
                        + "(at_ID, at_Name, ratio) "
                        + "VALUES (NULL, '" + at_Name + "'," + ratio + " );";
                System.out.println(q);
                statement.execute(q);
                return true;
            } catch (Exception ex) {
                ex.printStackTrace();
                closeConnection(con);
            }
        }
        return false;
    }
    /* --------------------------------------------------------- */

    public int[] getdep_IDs() {
        int deps[] = new int[getDepartmentQuantity()];
        Connection con = connectToDB(USERNAME, PASSWORD);
        if (con != null) {
            try {
                Statement statement = con.createStatement();
                ResultSet result = statement.executeQuery("SELECT dep_ID"
                        + " FROM department"
                        + " ORDER BY dep_Name");
                for (int i = 0; i < deps.length; i++) {
                    result.next();
                    deps[i] = result.getInt("dep_ID");
                }
                closeConnection(con);
                return deps;
            } catch (Exception e) {
            }
        }
        return null;
    }

    public int[] getAT_IDs() {
        int ATs[] = new int[getActivityTypeQuantity()];
        Connection con = connectToDB(USERNAME, PASSWORD);
        if (con != null) {
            try {
                Statement statement = con.createStatement();
                ResultSet result = statement.executeQuery("SELECT at_ID"
                        + " FROM activity_type"
                        + " ORDER BY at_ID");
                for (int i = 0; i < ATs.length; i++) {
                    result.next();
                    ATs[i] = result.getInt("at_ID");
                }
                closeConnection(con);
                return ATs;
            } catch (Exception ex) {
                ex.printStackTrace();
                closeConnection(con);
            }
        }
        return null;
    }

    public String[] getAT_Names() {
        String ATs[] = new String[getActivityTypeQuantity()];
        Connection con = connectToDB(USERNAME, PASSWORD);
        if (con != null) {
            try {
                Statement statement = con.createStatement();
                ResultSet result = statement.executeQuery("SELECT at_Name"
                        + " FROM activity_type"
                        + " ORDER BY at_ID");
                for (int i = 0; i < ATs.length; i++) {
                    result.next();
                    ATs[i] = result.getString("AT_Name");
                }
                closeConnection(con);
                return ATs;
            } catch (Exception e) {
                closeConnection(con);
            }
        }
        return null;
    }

    public String[] getdep_Names() {
        String deps[] = new String[getDepartmentQuantity()];
        Connection con = connectToDB(USERNAME, PASSWORD);
        if (con != null) {
            try {
                Statement statement = con.createStatement();
                ResultSet result = statement.executeQuery("SELECT dep_Name"
                        + " FROM department"
                        + "  ORDER BY dep_Name");
                for (int i = 0; i < deps.length; i++) {
                    result.next();
                    deps[i] = result.getString("dep_Name");
                }
                closeConnection(con);
                return deps;
            } catch (Exception ex) {
                ex.printStackTrace();
                closeConnection(con);
            }
        }
        return null;
    }

    public double getATRatio(int at_ID) {
        Connection con = connectToDB(USERNAME, PASSWORD);
        if (con != null) {
            ResultSet result;
            String query = "SELECT ratio"
                    + " FROM activity_type"
                    + " WHERE at_ID=" + at_ID;
            try {
                Statement statement = con.createStatement();
                result = statement.executeQuery(query);
                result.next();
                return result.getDouble("ratio");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return -1;
    }

    public double[] getATRatios() {
        Connection con = connectToDB(USERNAME, PASSWORD);
        if (con != null) {
            double ratio[];
            ResultSet result;
            String query = "SELECT count(at_ID) FROM activity_type";
            try {
                Statement statement = con.createStatement();
                result = statement.executeQuery(query);
                result.next();
                ratio = new double[result.getInt("ratio")];
            } catch (Exception ex) {
                return null;
            }
            query = "SELECT ratio"
                    + " FROM activity_type"
                    + " ORDER BY at_ID";
            try {
                Statement statement = con.createStatement();
                result = statement.executeQuery(query);
                for (int i = 0; i < 11; i++) {
                    result.next();
                    ratio[i] = result.getDouble("ratio");
                }
                closeConnection(con);
            } catch (SQLException ex) {
                ex.printStackTrace();
                closeConnection(con);
                return null;
            }
            return ratio;
        } else {
            return null;
        }
    }
    /* --------------------------------------------------------- */

    public int getDepartmentQuantity() {
        Connection con = connectToDB(USERNAME, PASSWORD);
        if (con != null) {
            try {
                Statement statement = con.createStatement();
                ResultSet result = statement.executeQuery(
                        " SELECT COUNT(dep_ID)"
                        + " FROM department");
                result.next();
                int depNum = result.getInt("COUNT(dep_ID)");
                closeConnection(con);
                return depNum;
            } catch (Exception ex) {
            }
        }
        return -1;
    }

    public int getActivityTypeQuantity() {
        Connection con = connectToDB(USERNAME, PASSWORD);
        if (con != null) {
            try {
                Statement statement = con.createStatement();
                ResultSet result = statement.executeQuery(
                        " SELECT COUNT(at_ID)"
                        + " FROM activity_type");
                result.next();
                int acNum = result.getInt("COUNT(at_ID)");
                closeConnection(con);
                return acNum;
            } catch (Exception ex) {
            }
        }
        return -1;
    }
    /* --------------------------------------------------------- */

    private boolean createUser(String username, String password) {
        Connection con = connectToServer(ROOT_ACCOUNT, ROOT_PASSWORD);
        if (con != null) {
            String q;
            Statement statement;
            try {
                statement = con.createStatement();
                q = "DROP USER '" + username + "'@'localhost'";
                statement.execute(q);
            } catch(Exception ex) {
            }
            try {
                statement = con.createStatement();
                q = "CREATE USER '" + username + "'@'localhost'"
                        + " IDENTIFIED BY '" + password + "';";
                statement.execute(q);
            } catch (Exception ex) {
            }
            try {
                statement = con.createStatement();
                q = " GRANT ALL PRIVILEGES ON " + DATABASE
                        + ".* to " + username + "@localhost ;";
                statement.execute(q);
                closeConnection(con);
                return true;
            } catch (Exception ex) {

//                ex.printStackTrace();
                closeConnection(con);
            }
        } else {
        }
        return false;
    }

    private boolean deleteUser(String username) {
        Connection con = connectToServer(ROOT_ACCOUNT, ROOT_PASSWORD);
        if (con != null) {
            try {
                String q = "DROP USER " + username + "@localhost ;";
                Statement statement = con.createStatement();
                statement.execute(q);
                closeConnection(con);
                return true;
            } catch (Exception ex) {
                ex.printStackTrace();
                closeConnection(con);
            }
        } else {
        }
        return false;
    }

    private boolean deleteDatabase(String database) {
        Connection con = connectToServer(ROOT_ACCOUNT, ROOT_PASSWORD);
        if (con != null) {
            try {
                String q = "DROP DATABASE " + database + ";";
                Statement statement = con.createStatement();
                statement.execute(q);
                closeConnection(con);
                return true;
            } catch (Exception ex) {
                ex.printStackTrace();
                closeConnection(con);
            }
        } else {
        }
        return false;
    }

    public void changePassword(String pass) {
        Connection con = connectToServer(USERNAME, PASSWORD);
        Statement stmt;
        String q;
        try {
            stmt = con.createStatement();
            q = "set password = password('" + pass + "');";
            stmt.executeQuery(q);
            setPassword(pass);
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }
    public void createRootPassword() {
        Connection con = connectToServer(ROOT_ACCOUNT, "");
        if (con == null) {
            con = connectToServer(ROOT_ACCOUNT, ROOT_PASSWORD);
        }
        if (con != null) {
            String q;
            Statement statement;
            try {
                statement = con.createStatement();
                q = "set password = password('" + ROOT_PASSWORD + "');";
                statement.executeQuery(q);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

    }

    public boolean clearEverything() {
        boolean userDeleted = deleteUser(USERNAME);
        boolean databaseDeleted = deleteDatabase(DATABASE);
        return databaseDeleted && userDeleted;

    }

    public void importDatabase(String sourcePath) throws Exception {
        Connection con = connectToDB(USERNAME, PASSWORD);
        String q = "";
        File f = new File(sourcePath);
        InputStreamReader isr = new InputStreamReader(
                new FileInputStream(f), Charset.forName("UTF-8"));
        int buff = isr.read();
        char[] a = new char[1];
        while (buff != -1) {
            a[0] = (char) buff;
            q = q + new String(a);
            buff = isr.read();
        }
        String[] commands = q.split(";");
        Statement statement = con.createStatement();
        for (String s : commands) {
            statement.execute(s);
        }
        closeConnection(con);
    }

    public void exportDatabase(String destinationPath) {
        String dumperPath =
                (String) Main.get_Wamp_EXE_File_Address().subSequence(
                0, Main.get_Wamp_EXE_File_Address().length()
                - Strings.WAMP_STARTER.length())
                + Main.getDupmerRelativePath();

        String[] cmdarray = new String[4];
        cmdarray[0] = dumperPath;
        cmdarray[1] = "--user=" + USERNAME;
        cmdarray[2] = "--password=" + PASSWORD;
        cmdarray[3] = DATABASE;
        try {
            Process p = Runtime.getRuntime().exec(cmdarray);
            Scanner sc = new Scanner(p.getInputStream());
            File output = new File(destinationPath);
            FileWriter fw = new FileWriter(output);
            while (sc.hasNextLine()) {
                fw.write(sc.nextLine() + "\n");
            }
            fw.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public boolean initiateDataBase() {
        createRootPassword();
        Connection con = connectToServer(ROOT_ACCOUNT, ROOT_PASSWORD);
        if (con != null) {
            String q = "";
            Statement stmt;
            try {
                q = "CREATE DATABASE IF NOT EXISTS " + DATABASE;
                stmt = con.createStatement();
                stmt.executeUpdate(q);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            closeConnection(con);
            createUser(USERNAME, PASSWORD);
            try {
                con = connectToDB(USERNAME, PASSWORD);
                q = "DROP TABLE activity;";
                stmt = con.createStatement();
                stmt.executeUpdate(q);
                System.out.println("Table 'activity' Droped");
            } catch (Exception ex) {
                System.out.println("Could not drop 'activity'");
            }
            try {
                q = "DROP TABLE activity_type;";
                stmt = con.createStatement();
                stmt.executeUpdate(q);
                System.out.println("Table 'activity_type' Droped;");
            } catch (Exception ex) {
                System.out.println("Could not drop 'activity_type'");
            }
            try {
                q = "DROP TABLE department;";
                stmt = con.createStatement();
                stmt.executeUpdate(q);
                System.out.println("Table 'department' Droped;");
            } catch (Exception ex) {
                System.out.println("Could not drop 'department'");
            }
            try {
                q = "DROP TABLE wallpaperfilepath";
                stmt = con.createStatement();
                stmt.executeUpdate(q);
                System.out.println("Table 'wallpaperfilepath' Droped;");
            } catch (Exception ex) {
                System.out.println("Could not drop 'wallpaperfilepath");
            }
            closeConnection(con);
            try {
                con = connectToDB(USERNAME, PASSWORD);
                q = "CREATE TABLE activity("
                        + "ac_ID int PRIMARY KEY AUTO_INCREMENT,"
                        + "dep_ID int NOT NULL,"
                        + "at_ID int NOT NULL,"
                        + "point int NOT NULL,"
                        + "ac_Name text NOT NULL,"
                        + "day int NOT NULL,"
                        + "month int NOT NULL,"
                        + "year int NOT NULL"
                        + ") COLLATE utf8_persian_ci;";
                stmt = con.createStatement();
                stmt.executeUpdate(q);
                q = "CREATE TABLE activity_type("
                        + "at_ID int PRIMARY KEY AUTO_INCREMENT,"
                        + "at_Name varchar(150) NOT NULL,"
                        + "ratio double NOT NULL"
                        + ") COLLATE utf8_persian_ci ;";
                stmt = con.createStatement();
                stmt.executeUpdate(q);
                q = "CREATE TABLE department("
                        + "dep_ID int PRIMARY KEY AUTO_INCREMENT,"
                        + "dep_Name varchar(150) NOT NULL) COLLATE utf8_persian_ci;";
                stmt = con.createStatement();
                stmt.executeUpdate(q);
                q = "CREATE TABLE wallpaperfilepath("
                        + "ID int PRIMARY KEY,"
                        + "FilePath varchar(10000) NOT NULL) COLLATE latin1_swedish_ci;";
                stmt = con.createStatement();
                stmt.executeUpdate(q);
                closeConnection(con);
                return true;
            } catch (Exception ex) {
                //PROBABLY DATABASE ALREADY EXISTS.
                closeConnection(con);
            }
        }
        return false;
    }

    private Connection connectToServer(String username, String password) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://localhost:3306/";
            Properties objProperties = new Properties();
            objProperties.put("user", username);
            objProperties.put("password", password);
            objProperties.put("useUnicode", "true");
            objProperties.put("characterEncoding", "utf-8");
            Connection con = DriverManager.getConnection(url, objProperties);
            return con;
        } catch (Exception ex) {
            System.out.println("Connection to wamp server failed.");
            ex.printStackTrace();
            return null;
        }
    }

    private Connection connectToDB(String username, String password) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://localhost:3306/" + DATABASE;
            Properties objProperties = new Properties();
            objProperties.put("user", username);
            objProperties.put("password", password);
            objProperties.put("useUnicode", "true");
            objProperties.put("characterEncoding", "utf-8");

            Connection con = DriverManager.getConnection(url, objProperties);
            return con;
        } catch (Exception ex) {
            System.out.println("Connection to sql database failed.");
            ex.printStackTrace();
            return null;
        }
    }

    private boolean closeConnection(Connection con) {
        try {
            con.close();
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean isConnected() {
        return connectToServer(ROOT_ACCOUNT, ROOT_PASSWORD) != null
                || connectToServer(ROOT_ACCOUNT, "") != null;
    }

    /* --------------------------------------------------------- */
    public boolean setDep_Name(int dep_ID, String dep_Name) {
        Connection con = connectToDB(USERNAME, PASSWORD);
        if (con != null) {
            String query = "UPDATE department SET dep_Name = '" + dep_Name + "' WHERE dep_ID=" + dep_ID;
            try {
                Statement statement = con.createStatement();
                statement.execute(query);
                closeConnection(con);
                return true;
            } catch (Exception ex) {
            }
        }
        return false;
    }

    public boolean setAT_Name(int at_ID, String at_Name) {
        Connection con = connectToDB(USERNAME, PASSWORD);
        if (con != null) {
            String query = "UPDATE activity_type SET at_Name = '" + at_Name + "' WHERE at_ID=" + at_ID;
            try {
                Statement statement = con.createStatement();
                statement.execute(query);
                closeConnection(con);
                return true;
            } catch (Exception ex) {
            }
        }
        return false;
    }

    public boolean setAT_ratio(int at_ID, double ratio) {
        Connection con = connectToDB(USERNAME, PASSWORD);
        if (con != null) {
            String query = "UPDATE activity_type SET ratio = " + ratio + " WHERE at_ID=" + at_ID;
            try {
                Statement statement = con.createStatement();
                statement.execute(query);
                closeConnection(con);
            } catch (Exception ex) {
                ex.printStackTrace();
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    public double getAT_ratio(int at_ID) {
        Connection con = connectToDB(USERNAME, PASSWORD);
        if (con != null) {
            ResultSet result;
            Statement statement;
            try {
                statement = con.createStatement();
                result = statement.executeQuery("SELECT ratio "
                        + "FROM activity_type "
                        + "WHERE at_ID = " + at_ID);
                result.next();
                double ratio = result.getDouble("ratio");
                closeConnection(con);
                return ratio;
            } catch (Exception e) {
                return -1;
            }
        } else {
            return -1;
        }
    }

    public String getAT_Name(int at_ID) {
        Connection con = connectToDB(USERNAME, PASSWORD);
        if (con != null) {
            ResultSet result;
            Statement statement;
            try {
                statement = con.createStatement();
                result = statement.executeQuery("SELECT at_Name "
                        + "FROM activity_type "
                        + "WHERE at_ID = " + at_ID);
                result.next();
                String name = result.getString("at_Name");
                closeConnection(con);
                return name;
            } catch (Exception e) {
                return "";
            }
        } else {
            return "";
        }
    }

    public String getDepName(int dep_ID) {
        Connection con = connectToDB(USERNAME, PASSWORD);
        if (con != null) {
            ResultSet result;
            Statement statement;
            try {
                statement = con.createStatement();
                result = statement.executeQuery("SELECT dep_Name " 
                        + "FROM department " 
                        + "WHERE dep_ID = " 
                        + dep_ID);
                result.next();
                String name = result.getString("dep_Name");
                closeConnection(con);
                return name;
            } catch (SQLException ex) {
                return "";
            }
        } else {
            return "";
        }
    }

    public Activity getActivity(int activityID) {
        Connection con = connectToDB(USERNAME, PASSWORD);
        if (con != null) {
            ResultSet result;
            Statement statement;
            Activity act = null;
            try {
                statement = con.createStatement();
                result = statement.executeQuery("SELECT *"
                        + "FROM activity "
                        + "WHERE ac_ID = " + activityID);
                result.next();
                act = new Activity();
                act.setSubject(result.getString("ac_Name"));
                try {
                    act.setDate(new Date(result.getInt("year"), result.getInt("month"), result.getInt("day")));
                } catch (Exception ex) {
                    System.out.println("A wrong date has been saved in the DB");
                    ex.printStackTrace();
                }
                act.setDepartmentID(result.getInt("dep_ID"));
                act.setTypeID(result.getInt("at_ID"));
                act.setPoint(result.getInt("point"));
                act.setID(activityID);
                closeConnection(con);
                return act;
            } catch (SQLException ex) {
                return null;
            }
        } else {
            return null;
        }
    }
    /* --------------------------------------------------------- */

    public boolean insertActivity(Activity a) {
        Connection con = connectToDB(USERNAME, PASSWORD);
        if (con != null) {
            try {
                Statement statement = con.createStatement();
                String q = "INSERT INTO activity (dep_ID, at_ID, point, ac_Name, day , month, year)"
                        + " values ("
                        + a.getDepartmentID()
                        + "," + a.getTypeID()
                        + "," + a.getPoint()
                        + "," + " '" + a.getSubject() + "' "
                        + "," + a.getDate().getDay()
                        + "," + a.getDate().getMonth()
                        + "," + a.getDate().getYear()
                        + ")";
                statement.execute(q);
                closeConnection(con);
            } catch (Exception ex) {
                ex.printStackTrace();
                closeConnection(con);
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    public void updateActivity(Activity act) {
        System.out.println(act);
        Connection con = connectToDB(USERNAME, PASSWORD);
        if (con != null) {
            Statement statement;
            try {
                statement = con.createStatement();
                statement.execute("UPDATE activity"
                        + " SET dep_ID = " + act.getDepartmentID() + ""
                        + ", at_ID = " + act.getTypeID() + ""
                        + ", point = " + act.getPoint() + ""
                        + ", ac_Name = '" + act.getSubject() + "'"
                        + ", day = " + act.getDate().getDay() + ""
                        + ", month = " + act.getDate().getMonth() + ""
                        + ", year = " + act.getDate().getYear() + ""
                        + " WHERE ac_ID = " + act.getID());
                closeConnection(con);
                System.out.println("update done");
            } catch (Exception e) {
                System.out.println("update undone. Exception Occured");
                e.printStackTrace();
            }
        }
    }

    public Result giveDetailedAnswer(Query q) {
        Connection con = connectToDB(USERNAME, PASSWORD);

        if (con != null) {
            System.out.println("CONNECTED");
            ResultSet result;
//            String finalAnswer = "";
            String dateRestrictions = "";
            dateRestrictions += " and ( ";
            dateRestrictions +=
                    "((year<" + q.getFinishLimit().getYear() + ") OR"
                    + "(year=" + q.getFinishLimit().getYear() + " and " + "month<" + q.getFinishLimit().getMonth() + ") OR"
                    + "(year=" + q.getFinishLimit().getYear() + " and month=" + q.getFinishLimit().getMonth() + " and day<=" + q.getFinishLimit().getDay() + ") )"
                    + " and ("
                    + "(year>" + q.getStartLimit().getYear() + ") OR"
                    + "(year=" + q.getStartLimit().getYear() + " and " + "month>" + q.getStartLimit().getMonth() + ") OR"
                    + "(year=" + q.getStartLimit().getYear() + " and month=" + q.getStartLimit().getMonth() + " and day>=" + q.getStartLimit().getDay() + ") )";
            ;
            dateRestrictions += ")";


            try {
                Statement statement = con.createStatement();
                Result finalAnswer = new Result();
                for (int i = 0; i < q.getDepartmentIDs().length; i++) {
                    String query = "";
                    for (int j = 0; j < q.getActivityTypeIDs().length; j++) {
                        query = "SELECT * FROM activity WHERE dep_ID="
                                + q.getDepartmentIDs()[i] + " AND at_ID=" + q.getActivityTypeIDs()[j];
                        query += dateRestrictions;
                        result = statement.executeQuery(query);
                        while (result.next()) {
                            Tuple_of_Ac t = new Tuple_of_Ac();
                            t.setAt_id(result.getInt("at_ID"));
                            t.setDate(new Date(result.getInt("year"), result.getInt("month"), result.getInt("day")));
                            t.setDep_id(result.getInt("dep_ID"));
                            t.setAc_name(result.getString("ac_Name"));
                            t.setAc_id(result.getInt("ac_ID"));
                            t.setPoint(result.getInt("point"));
                            finalAnswer.addTuple(t);
                        }
                    }
                }
                closeConnection(con);
                return finalAnswer;
            } catch (Exception ex) {
                ex.printStackTrace();
                closeConnection(con);
                return null;
            }
        } else {
            // JOptionPane Error
            return null;
        }
    }

    private String getDateRestrictions(Query q) {
        String dateRestrictions = "";


        return dateRestrictions += " AND ( "
                + "((year<" + q.getFinishLimit().getYear() + ") OR"
                + "(year=" + q.getFinishLimit().getYear() + " and " + "month<" + q.getFinishLimit().getMonth() + ") OR"
                + "(year=" + q.getFinishLimit().getYear() + " and month=" + q.getFinishLimit().getMonth() + " and day<=" + q.getFinishLimit().getDay() + ") )"
                + " and ("
                + "(year>" + q.getStartLimit().getYear() + ") OR"
                + "(year=" + q.getStartLimit().getYear() + " and " + "month>" + q.getStartLimit().getMonth() + ") OR"
                + "(year=" + q.getStartLimit().getYear() + " and month=" + q.getStartLimit().getMonth() + " and day>=" + q.getStartLimit().getDay() + ") )"
                + ")";



    }

    public OverallResult giveOverallAnswer(Query q) {
        Connection con = connectToDB(USERNAME, PASSWORD);


        if (con != null) {
            try {
                Integer results[][] = new Integer[q.getActivityTypeIDs().length][q.getDepartmentIDs().length];
                ResultSet result;
                String query;
                Statement statement = con.createStatement();

                OverallResult res = new OverallResult();
                Tuple_of_ACCount temp;
                for (int i = 0; i < q.getActivityTypeIDs().length; i++) {
                    for (int j = 0; j < q.getDepartmentIDs().length; j++) {
                        results[i][j] = new Integer(0);
                        query =
                                "SELECT "
                                + "COUNT(ac_ID), Sum(point)"
                                + "FROM activity "
                                + "WHERE dep_ID="
                                + q.getDepartmentIDs()[j]
                                + " AND at_ID="
                                + q.getActivityTypeIDs()[i]
                                + getDateRestrictions(q);
                        result = statement.executeQuery(query);
                        result.next();

                        temp = new Tuple_of_ACCount();
                        temp.setAt_ID(q.getActivityTypeIDs()[i]);
                        temp.setDep_ID(q.getDepartmentIDs()[j]);
                        temp.setNum(result.getInt("COUNT(ac_ID)"));
                        temp.setGradeSum(getATRatio(q.getActivityTypeIDs()[i]) * result.getInt("Sum(point)"));
                        res.addTuple(temp);
                    }
                }
                closeConnection(con);
                return res;
            } catch (Exception ex) {
                ex.printStackTrace();
                closeConnection(con);
                return null;
            }
        } else {
            return null;
        }
    }

    public boolean deleteActivity(int ac_ID) throws Exception {
        Connection con = connectToDB(USERNAME, PASSWORD);
        if (con != null) {
            try {
                Statement statement = con.createStatement();
                String query = ""
                        + "DELETE "
                        + "FROM "
                        + "activity "
                        + "WHERE ac_ID=" + ac_ID;
                statement.execute(query);
                closeConnection(con);
            } catch (SQLException ex) {
                closeConnection(con);
                return false;
            }
        } else {
            throw new Exception("connection failed");
        }
        return true;
    }
}
