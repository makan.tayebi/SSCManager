/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DataStructure;

/**
 *
 * @author Makan Tayebi
 */
public class Date {

    private int day;
    private int month;
    private int year;

    public Date(int year, int month, int day) throws Exception {
        setDay(day);
        setMonth(month);
        setYear(year);
    }

    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    public void setDay(int day) throws Exception {
        if (day < 1 || day > 31 || (day > 30 && 0 < month && month < 6)) {
            throw new Exception();
        }
        this.day = day;
    }

    public void setMonth(int month) throws Exception {
        if (month < 1 || month > 12) {
            throw new Exception();
        }
        this.month = month;
    }

    public void setYear(int year) throws Exception {
        if (year < 1) {
            throw new Exception();
        }
        this.year = year;
    }

    public boolean equals(Date d) {
        return d.getDay() == day && d.getMonth() == month && d.getYear() == year;
    }

    public Date setDate(int year, int month, int day) throws Exception {
        setDay(day);
        setMonth(month);
        setYear(year);
        return this;
    }

    /**
     * @return true if Date d is someday after this date. false
     * otherwise.
     */
    public boolean isBefore(Date d) {
        if (d.getYear() > year) {
            return true;
        }
        if (d.getYear() < year) {
            return false;
        }
        if (d.getMonth() > month) {
            return true;
        }
        if (d.getMonth() < month) {
            return false;
        }
        if (d.getDay() > day) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return getYear() + "/" + getMonth() + "/" + getDay();
    }

    public String toString_Reverse() {
        return getDay() + "/" + getMonth() + "/" + getYear();
    }
}
