/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DataStructure;

/**
 *
 * @author Makan Tayebi
 */
public class Result {

    Tuple_of_Ac[] table;
    int wHead = 0, rHead = 0;

    public Result() {
        table = new Tuple_of_Ac[16];
    }

    public Tuple_of_Ac sum() {
        Tuple_of_Ac t = new Tuple_of_Ac();
        int pointSum = 0;
        for (int i = 0; i < rHead; i++) {
            pointSum += table[i].getPoint();
        }
        t.setPoint(pointSum);
        return t;
    }

    private boolean tableFull() {
        return wHead == table.length;
    }

    private void extend() {
        Tuple_of_Ac[] temp = new Tuple_of_Ac[table.length * 2];
        for (int i = 0; i < table.length; i++) {
            temp[i] = table[i];
        }
        table = temp;
        System.gc();
    }

    public void reset() {
        rHead = 0;
    }

    public int size() {
        return wHead;
    }

    public Tuple_of_Ac next() {
        if (rHead < wHead) {
            rHead++;
            return table[rHead - 1];
        }
        return null;
    }

    public void addTuple(Tuple_of_Ac t) {
        if (tableFull()) {
            extend();
        }
        table[wHead] = t;
        wHead++;
    }
    public void sortByDate(){
        Tuple_of_Ac temp;
        for (int i = 0; i < wHead; i++) {
            for (int j = 0; j < i; j++) {
                if(table[i].getDate().isBefore(table[j].getDate())){
                    temp = table[i];
                    table[i] = table [j];
                    table [j] = temp;
                    System.out.println("A change was made.");
                }
            }
        }
    }
}
