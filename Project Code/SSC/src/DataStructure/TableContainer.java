/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package DataStructure;

import java.awt.LayoutManager;
import javax.swing.JPanel;

/**
 *
 * @author Makan
 */
public class TableContainer extends JPanel {
    private MyTable table;

    public TableContainer(LayoutManager l) {
        super(l);
    }

    public MyTable getTable() {
        return table;
    }

    public void setTable(MyTable table) {
        this.table = table;
    }
}