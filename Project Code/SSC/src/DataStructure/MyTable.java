/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DataStructure;

import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author Makan Tayebi
 */
public class MyTable extends JTable {

    /**
     * additional data about table.
     */
    private Date sDate, fDate;
    /**
     * additional data about table.
     */
    private String dep_Name;
    /**
     * additional data about table.
     */
    private String at_Name;
    public MyTable(int numRows, int numColumns) {
        super(numRows, numColumns);
        sDate = null;
        fDate = null;
        dep_Name = null;
        at_Name = null;
    }

    public void setAt_Name(String at_Name) {
        this.at_Name = at_Name;
    }

    public void setDep_Name(String dep_Name) {
        this.dep_Name = dep_Name;
    }

    public void setfDate(Date fDate) {
        this.fDate = fDate;
    }

    public void setsDate(Date sDate) {
        this.sDate = sDate;
    }

    public MyTable() {
        super();
    }

    @Override
    public Component prepareRenderer(TableCellRenderer renderer,
            int rowIndex, int vColIndex) {
        Component c = super.prepareRenderer(renderer, rowIndex, vColIndex);
        c.setForeground(Color.RED);
        if (rowIndex % 2 == 0) {
            c.setBackground(Color.LIGHT_GRAY);
        }
        c.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        return c;
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }
}
