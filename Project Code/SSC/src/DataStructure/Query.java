/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DataStructure;

/**
 *
 * @author Makan Tayebi
 */
public class Query {

    private Date startLimit, finishLimit;
    private int departmentIDs[], activityTypeIDs[];

    public void setActivityTypeID(int index, int val) {
        activityTypeIDs[index] = val;
    }

    public void setDepartmentID(int index, int val) {
        departmentIDs[index] = val;
    }

    public void setActivityTypeIDs(int[] activityTypeIDs) {
        this.activityTypeIDs = activityTypeIDs;
    }

    public void setDepartmentIDs(int[] departmentIDs) {
        this.departmentIDs = departmentIDs;
    }

    public int[] getActivityTypeIDs() {
        return activityTypeIDs;
    }

    public int[] getDepartmentIDs() {
        return departmentIDs;
    }

    public Query() {
        departmentIDs = null;
        activityTypeIDs = null;
        startLimit = null;
        finishLimit = null;
    }

    public Date getFinishLimit() {
        return finishLimit;
    }

    public Date getStartLimit() {
        return startLimit;
    }

    public void setFinishLimit(Date FinishLimit) {
        this.finishLimit = FinishLimit;
    }

    public void setStartLimit(Date startLimit) {
        this.startLimit = startLimit;
    }
}
