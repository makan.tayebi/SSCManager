/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DataStructure;

/**
 *
 * @author Makan Tayebi
 */
public class Tuple_of_Ac {

    private int dep_id, at_id, points, ac_id;
    private Date date;
    private String ac_name;
    private int point;
    public Tuple_of_Ac() {
        dep_id = -1;
        at_id = -1;
        points = -1;
        ac_id = -1;
        point = -1;
        date = null;
        ac_name= null;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public void setAc_name(String ac_name) {
        this.ac_name = ac_name;
    }

    public void setAc_id(int ac_id) {
        this.ac_id = ac_id;
    }

    public void setAt_id(int at_id) {
        this.at_id = at_id;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setDep_id(int dep_id) {
        this.dep_id = dep_id;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getAt_id() {
        return at_id;
    }

    public Date getDate() {
        return date;
    }

    public String getAc_name() {
        return ac_name;
    }

    public int getAc_id() {
        return ac_id;
    }

    public int getDep_id() {
        return dep_id;
    }

    public int getPoints() {
        return points;
    }

    public double getPoint() {
        return point;
    }

    @Override
    public String toString() {
        return "date: " + date;
    }
    
}
