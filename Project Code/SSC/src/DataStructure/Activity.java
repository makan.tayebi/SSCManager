/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DataStructure;

/**
 *
 * @author Makan Tayebi
 */
public class Activity {

    private Date date;
    private int point;
    private int typeID, departmentID;
    private String subject;
    private int ID;

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getID() {
        return ID;
    }

    public void setDepartmentID(int departmentID) {
        this.departmentID = departmentID;
    }

    public int getDepartmentID() {
        return departmentID;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public double getPoint() {
        return point;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public void setTypeID(int typeID) {
        this.typeID = typeID;
    }

    public int getTypeID() {
        return typeID;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSubject() {
        return subject;
    }

    @Override
    public String toString() {
        return "Name: " + getSubject() + "\n"
                + "ID: " + getID() + "\n"
                + "Date: " + getDate() + "\n"
                + "Department: " + getDepartmentID() + "\n"
                + "Point: " + getPoint() + "\n"
                + "Type ID: " + getTypeID() + "\n";
    }
}
