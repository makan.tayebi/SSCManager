/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DataStructure;

/**
 *
 * @author Makan Tayebi
 */
public class OverallResult {

    Tuple_of_ACCount[] table;
    int wHead = 0, rHead = 0;

    public int getCount_Dep(int depID) {
        int count = 0;
        for (int i = 0; i < rHead; i++) {
            if (table[i].getDep_ID() == depID) {
                count += table[i].getNum();
            }
        }
        return count;
    }

    public double getGradeSum_Dep(int dep_ID) {
        double gradeSumSum = 0;
        for (int i = 0; i < rHead; i++) {
            if (table[i].getDep_ID() == dep_ID) {
                gradeSumSum += table[i].getGradeSum();
            }
        }
        return gradeSumSum;
    }

    public int getCount_AT(int at_ID) {
        int count = 0;
        for (int i = 0; i < rHead; i++) {
            if (table[i].getAt_ID() == at_ID) {
                count += table[i].getNum();
            }
        }
        return count;
    }

    public double getGradeSum_AT(int at_ID) {
        double gradeSumSum = 0;
        for(int i =0; i< rHead;i++){
            if(table[i].getAt_ID() == at_ID){
                gradeSumSum += table[i].getGradeSum();
            }
        }
        return gradeSumSum;
    }

    public OverallResult() {
        table = new Tuple_of_ACCount[16];
    }

    private boolean tableFull() {
        return wHead == table.length;
    }

    private void extend() {
        Tuple_of_ACCount[] temp = new Tuple_of_ACCount[table.length * 2];
        for (int i = 0; i < table.length; i++) {
            temp[i] = table[i];
        }
        table = temp;
        System.gc();
    }

    public void reset() {
        rHead = 0;
    }

    public int size() {
        return wHead;
    }

    public Tuple_of_ACCount next() {
        if (rHead < wHead) {
            rHead++;
            return table[rHead - 1];
        }
        return null;
    }

    public void addTuple(Tuple_of_ACCount t) {
        if (tableFull()) {
            extend();
        }
        table[wHead] = t;
        wHead++;
    }
}
