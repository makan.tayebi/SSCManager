/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DataStructure;

/**
 *
 * @author Makan Tayebi
 */
public class Tuple_of_ACCount {

    private int at_ID, Dep_ID, num;
    private double gradeSum;

    public void setAt_ID(int at_ID) {
        this.at_ID = at_ID;
    }

    public void setDep_ID(int Dep_ID) {
        this.Dep_ID = Dep_ID;
    }
    
    public void setGradeSum(double gradeSum) {
        this.gradeSum = gradeSum;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getAt_ID() {
        return at_ID;
    }

    public int getDep_ID() {
        return Dep_ID;
    }

    public double getGradeSum() {
        return gradeSum;
    }

    public int getNum() {
        return num;
    }
}
