/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ssc;

import Constants.Strings;
import DataStructure.Restricted;
import Presentation.GetPassword;
import Presentation.MainFrame;
import db_code.MySQLConnector;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 *
 * @author Emertat
 */
public class Main implements Restricted{

    private static SystemType SYSTEM_TYPE = SystemType.X86;
    private static MySQLConnector DBtester = new MySQLConnector();
    private static String X86DumperPath_relative =
            "bin\\mysql\\mysql5.5.16\\bin\\mysqldump.exe";
    private static String X64DumperPath_relative =
            "bin\\mysql\\mysql5.1.53\\bin\\mysqldump.exe";
    private static String WAMP_SERVER_X64 = "\"WampServer2.1d-x64.exe\"";
    private static String WAMP_SERVER_X86 = "\"WampServer2.2a-x32.exe\"";
    private static String JRE_X86 = "\"jre-7u3-windows-i586.exe\"";
    private static String JRE_X64 = "\"jdk.exe\"";
    private static String pathFile_path = "wampAddress.txt";

    /**
     * this function returns a text file's relative path, that contains the 
     * absolute path of wampmanager.exe
     * @return
     */
    public static String getRelativePathContainerName() {
        return pathFile_path;
    }

    private enum SystemType {
        X86, X64
    }

    public static String getWampServerInstallerFileName() {
        if (SYSTEM_TYPE == SystemType.X86) {
            return WAMP_SERVER_X86;
        } else {
            return WAMP_SERVER_X64;
        }
    }

    public static String getJREFileName() {
        if (SYSTEM_TYPE == SystemType.X86) {
            return JRE_X86;
        } else {
            return JRE_X64;
        }
    }

    @SuppressWarnings("static-access")
    public static void informMessage(String msg, Component c) {
        JOptionPane inform = new JOptionPane();
        JLabel label = new JLabel(msg);
        inform.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        inform.showMessageDialog(c, label, "", 1);
    }

    @SuppressWarnings("static-access")
    public static int confirmMessage(String msg, Component c) {
        JOptionPane confirm = new JOptionPane();
        confirm.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        return confirm.showConfirmDialog(c, msg);
    }

    @SuppressWarnings("static-access")
    public static void errorMessage(String msg, Component c) {
        JOptionPane error = new JOptionPane();
        JLabel errorLabel = new JLabel(msg);
        try {
//            errorLabel.setFont(Font.createFont(Font.TRUETYPE_FONT, new java.io.File(Strings.Font_BNazanin)).deriveFont(Font.BOLD, (float) (16)));
            errorLabel.setFont(new Font("tahoma", Font.BOLD, 12));
        } catch (Exception ex) {
        }
        error.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        error.showMessageDialog(c, errorLabel, Strings.MESSAGE_ERROR, 0);
    }

    public static String getDupmerRelativePath() {
        if (SYSTEM_TYPE == SystemType.X86) {
            return X86DumperPath_relative;
        } else { // system type is X64
            return X64DumperPath_relative;
        }
    }

    public static String get_Wamp_EXE_File_Address() {
        /*GAIN PATH OF WAMPSERVER FROM A TRUSTED FILE NAMED: wampAddress.txt .*/
        File af = new File("wampAddress.txt"); //address file.
        if (!af.exists()) {
            errorMessage(Strings.MESSAGE_NECESSARY_FILE_MISSING, null);
            return null;
        }
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File("wampAddress.txt")));
            return br.readLine();
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static void terminateWamp() {
        System.out.println("trying to close the program :");
        try {
            Runtime.getRuntime().exec("taskkill -f -im mysqld.exe");
            System.out.println("mysql server terminated.");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
            Runtime.getRuntime().exec("taskkill -f -im wampmanager.exe");
            System.out.println("wampmanager terminated.");
        } catch (IOException ex) {
            ex.printStackTrace();
            System.out.println("could not close wamp server");
        }
    }

    /**
     * This function tries to connect to database 10 times in 20 seconds.
     * If is able to connect, return true. If is not able to connect after 20 
     * seconds, returns false.
     * 
     * @return false, if it is not able to connect to database after 20 second.
     * true otherwise.
     */
    protected static boolean startWampManager() {
        try {
            Runtime.getRuntime().exec(get_Wamp_EXE_File_Address());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        for (int i = 0; i < 10; i++) {
            if (!DBtester.isConnected()) {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            } else {
                return true;
            }
        }
        errorMessage(Strings.MESSAGE_UNABLE_TO_CONNECT, null);
        return false;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        Installer.main(null);
        if (startWampManager()) {
            GetPassword gp = new GetPassword(new Main());
        }
    }
    
    @Override
    public void passwordSet(String pass) {
        DBtester.setPassword(pass);
        new MainFrame(DBtester);
    }

    @Override
    public void cancelAction() {
        terminateWamp();
    }
}
