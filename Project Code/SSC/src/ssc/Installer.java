/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ssc;

import Constants.Strings;
import Presentation.SetPassword;
import db_code.MySQLConnector;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Makan Tayebi
 */
public class Installer {

    private static FileWriter w = null;
    private static File af = null;
    private static MySQLConnector db;

    /**
     * this function assumes that the WampStarter is in folder: "wamp"
     * and when it finds this folder, returns such a path to WampStarter.
     * it returns null if there is no folder named "wamp", or searching time
     * exceeds given time.
     * @param MaxTimeMillis a time given in milliseconds, creating a time limit
     * for the duration of execution.
     * @return a File, which has the path like this: ...\wamp\wampmanager.exe
     */
    public static File AutoFindWamp(int MaxTimeMillis) {
        long starting_time = System.currentTimeMillis();
        File root[] = File.listRoots();
        File result = null;
        ArrayList<File> files = new ArrayList<File>();

        for (File f : root) {
            files.add(f);
        }
        while (!files.isEmpty()) {
            File f = files.get(0);
            files.remove(0);

            if (f.isDirectory()) {
                if (Strings.WAMP.equals(f.getName())
                        && new File(f.getAbsolutePath() + "\\" + Strings.WAMP_STARTER).exists()) {
                    return new File(f.getAbsolutePath() + "\\" + Strings.WAMP_STARTER);
                } else {
                    File[] children = f.listFiles();
                    if (children != null) {
                        for (int i = 0; i < children.length; i++) {
                            files.add(children[i]);
                        }
                    }
                }
            }
            if (MaxTimeMillis <= System.currentTimeMillis() - starting_time) {
                return null;
            }
        }
        return result;
    }

    public static void main(String args[]) {
        File f = AutoFindWamp(300000); // 300seconds MAX
        if (f == null) {
            Main.errorMessage(Strings.MESSAGE_NECESSARY_FILE_MISSING, null);
        }
        set_Wamp_EXE_File_Address(f.getAbsolutePath());
        Main.startWampManager();
        System.out.println("wampmanager started successfuly");
        SetPassword sp = new SetPassword(null);
        while (sp.getPassword() == "") {
            try {
                Thread.sleep(10000); // check every 10seconds if user has entered pass.
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        db = new MySQLConnector();
        db.setPassword(sp.getPassword());
        db.initiateDataBase();
        Main.terminateWamp();
        try {
            Runtime.getRuntime().exec("cmd /c DEL " + Main.getJREFileName());
            Runtime.getRuntime().exec("cmd /c DEL " + Main.getWampServerInstallerFileName());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        System.out.println("Installer done");
    }
    
    private static boolean set_Wamp_EXE_File_Address(String add) {
        af = new File(Main.getRelativePathContainerName());
        try {
            if (!af.exists()) {
                af.createNewFile();
                System.out.println("created file.");
            }
            w = new FileWriter(af);
            w.write(add);
            w.close();
            return true;
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }
    }
}
