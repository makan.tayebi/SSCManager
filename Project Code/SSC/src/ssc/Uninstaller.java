/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ssc;

import db_code.MySQLConnector;

/**
 *
 * @author Emertat
 */
public class Uninstaller {
    private static MySQLConnector DB = new MySQLConnector();
    public static void main(String args[]) {
        Main.startWampManager();
        DB.clearEverything();
        Main.terminateWamp();
    }
}