package ssc;

import DataStructure.Date;
import DataStructure.Result;
import DataStructure.Tuple_of_Ac;
import java.sql.Connection;
import java.sql.Statement;
import db_code.MySQLConnector;
import java.io.IOException;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.print.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.DriverManager;
import java.text.*;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.border.BevelBorder;
import jxl.*;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

public class Test {

    private static void testDesktopPath() {
        System.out.println(System.getProperty("user.home") + "\\Desktop");
    }

    private static void fileTesting() {
        File[] roots = File.listRoots();
        for (File f : roots) {
            System.out.println(f.getAbsolutePath());
        }
    }

    private static void testStrings() {
        String sample = null;
        System.out.println("null is: " + sample);
        if ("" == null) {
            System.out.println("\"\" == null");
        }
    }

    private static void testValidityFunction() {
       MySQLConnector db = new MySQLConnector();
        System.out.println(db.isPassValid(""));
    }//tik

    private static void xlTest() {
        WritableWorkbook wwb = null;
        File f = new File("C:\\Users\\Emertat\\Desktop\\newFile");
        try {
            wwb = Workbook.createWorkbook(f);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

//        System.out.println(wwb.getNumberOfSheets());
//        Cell = wwb.getWritableCell()
        WritableSheet sheet = wwb.createSheet("MyFirstBuiltSheet", 1);
        try {
            sheet.addCell(new jxl.write.Label(1, 1, "محتوا of 1,1"));
        } catch (WriteException ex) {
            ex.printStackTrace();
        }
        try {
            wwb.write();
        } catch (IOException ex) {
        }
        try {
            wwb.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (WriteException ex) {
            ex.printStackTrace();
        }
    }

    private static void test1() {
        final String rows[][] = new String[2][12];
        final Object headers[] = {"YEK", "DO", "SE", "CHAHAR", "PANJ", "SHESH", "HAFT", "HASHT", "NOH", "DAH", "YAZDAH", "DAVAZDAH"};
        Runnable runner = new Runnable() {

            public void run() {
                rows[0][0] = "1";
                JFrame frame = new JFrame("Table Printing");
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                for (int i = 0; i < rows[0].length; i++) {
                    rows[0][i] = "" + i;
                    rows[1][i] = "" + i;
                }
                final JTable table = new JTable(rows, headers);
                for (int i = 0; i < rows[0].length; i++) {
                    table.getColumnModel().getColumn(i).setWidth(50);
                }
                System.out.println(table.getPreferredSize());
                JScrollPane scrollPane = new JScrollPane(table);
                frame.add(scrollPane, BorderLayout.CENTER);
                JButton button = new JButton("Print");
                ActionListener printAction = new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        try {
                            MessageFormat headerFormat = new MessageFormat("Page {0}");
                            MessageFormat footerFormat = new MessageFormat("- {0} -");
                            table.print(JTable.PrintMode.FIT_WIDTH, headerFormat, footerFormat);
                        } catch (PrinterException pe) {
                            System.err.println("Error printing: " + pe.getMessage());
                        }
                    }
                };
                button.addActionListener(printAction);
                frame.add(button, BorderLayout.SOUTH);
                frame.setSize(300, 150);
                frame.setVisible(true);
            }
        };
        EventQueue.invokeLater(runner);
    }

    public static void main(String args[]) {
        try {
            //        test1();
            //        test2();
            //        xlTest();
            //        testStrings();
            //        fileTesting();
            //        testDesktopPath();
            //        testGettingWampFile();
            //        testFileEncoding();
            //        testValidityFunction();
            //        changeRootPass();
            testTupleSort();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    private static void testTupleSort() throws Exception{
                    Result r = new Result();
                    Tuple_of_Ac t1 = new Tuple_of_Ac();
                    t1.setDate(new Date(1900, 1, 1));
                    Tuple_of_Ac t2 = new Tuple_of_Ac();
                    t2.setDate(new Date(1991, 1, 1));
                    r.reset();
                    r.addTuple(t2);
                    r.addTuple(t1);        
                    r.sortByDate();
                    System.out.println(r.next());
                    System.out.println(r.next());
    }
    private static void changeRootPass(){
        Connection con = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://localhost:3306/";
            Properties objProperties = new Properties();
            objProperties.put("user", "root");
            objProperties.put("password", "");
            objProperties.put("useUnicode", "true");
            objProperties.put("characterEncoding", "utf-8");
            con = DriverManager.getConnection(url, objProperties);
            //return con;
        } catch (Exception ex) {
            System.out.println("Connection to wamp server failed.");
            ex.printStackTrace();
        }
        String q;
        Statement stmt;
            try {
                q = "set password = password('" + "salam" + "');";
                stmt = con.createStatement();
                stmt.executeQuery(q);
            } catch(Exception ex) {
                ex.printStackTrace();
            }

    }
    private static void testFileEncoding() {
        BufferedReader br = null;
        try {
//            Main.errorMessage("یک متن فارسی", null);
            File f = new File("C:\\Users\\Emertat\\Desktop\\farsi.txt");
            br = new BufferedReader(new FileReader(f));
//            byte a[] = new byte[100];
            String s = new String(br.readLine().getBytes("UTF8"));
            Main.errorMessage(s, null);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private static void testGettingWampFile() {
        File f = Installer.AutoFindWamp(60000);
        if (f == null) {
            System.out.println("null");
            return;
        }
        System.out.println(f.getAbsolutePath());
    }

    private static void test2() {
        JFrame frame = new JFrame();
        frame.setVisible(true);
        frame.setSize(400, 100);
        frame.setLayout(new FlowLayout());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JMenuBar menu = new JMenuBar();
        menu.setSize(400, 100);
        menu.setLocation(0, 0);
        menu.setVisible(true);
        menu.setLayout(new GridLayout());
        menu.setBorder(new BevelBorder(1, Color.BLACK, Color.PINK));
        frame.add(menu);
        for (int i = 0; i < 2; i++) {
            menu.add(new JButton("Button" + i + 1));
//            menu.addSeparator();
            JSeparator temp = new JSeparator();
            temp.setOrientation(JSeparator.VERTICAL);
            temp.setVisible(true);
//            temp.setc
            menu.add(temp, "growy");
        }
//        menu.pack();
//        JSeparator sep = new JSeparator(JSeparator.VERTICAL);
//        sep.setUI();

    }
}
