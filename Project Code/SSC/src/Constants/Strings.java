/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Constants;

/**
 *
 * @author Makan Tayebi
 */
public class Strings {

    public static String ACTIVITY_TYPE = "نوع فالیت";
    public static String ACTIVITY_NAME = "نام فعالیت";
    public static String DEPARTMENT = "دانشکده";
    public static String AC_ID = "کد فعالیت";
    public static String NUMBER = "تعداد";
    public static String OPERATION_SUCCESSFUL = "عملیات با موفقیت انجام شد.";
    public static String POINTS_SLASH_NUMBERS = "امتیاز / تعداد";
    public static String TABLE_CELLS_CONTAIN = "خانه های حدول به فرمت زیر هستند:";
    public static String logoPath = "logo.png";
    public static String Font_BNazanin = "Bnazanin.ttf";
//    public static String Font_ArialBlack = "ariblk.ttf";
//    public static String Font_BSina = "BSinaBd.ttf";
    public static String contentFontName = "B Nazanin";
    public static String SUM = "جمع";
    public static String ALL = "همه";
    public static String WARNING = "اخطار";
    public static String SELECTION = "انتخابی";
    public static String DETAIL = " گزارش با جزئیات";
    public static String OVERALL_LABEL = "   گزارش کلی"; 
    public static String FROM_DATE_ = "از تاریخ";
    public static String TO_DATE_ = "تا تاریخ";
    public static String SUBMIT_BUTTON = "گزارش بگیر";
    /********************************title*****************************************/
    public static String TITLE_DEPMANAGER = "- مدیریت دانشکده ها";
    public static String TITLE_ATMANAGER = "تغییر ضرایب";
    public static String TITLE_MAINFRAME = "صفحه ی اصلی";
    public static String TITLE_DETAILEDRESULTFRAME = "نمایش جزئیات";
    public static String TITLE_NEWACTIVITYTASK = "ایجاد فعالیت جدید ";
    public static String TITLE_OVERALLRESULTFRAME = "اطلاعات کلی";
    public static String TITLE_QUERYFRAME = "ارائه اطلاعات";
    public static String TITLE_ABSTRACT_FRAME = "مدیریت فعالیت ها";
    public static String TITLE_SET_PASSWORD = "تعیین رمز عبور";
    public static String TITLE_OLD_PASSWORD = "رمز عبور قبلی";
    /********************************label*****************************************/
    public static String LABEL_SINGLE_DEPARTMENT = "فعالیتهای یک دانشکده";
    public static String LABEL_COPY_RIGHT = "Designed and developed by "
            + "Makan Tayebi \n";
    public static String LABEL_NAME = "عنوان";
    public static String LABEL_AT = "نوع فعالیت";
    public static String LABEL_RATIO = "ضریب";
    public static String LABEL_DATE = "تاریخ : ";
    public static String LABEL_DEP = "دانشکده:";
    public static String LABEL_POINT = "امتیاز:";
    public static String LABEL_PASSWORD_COLON = "رمز عبور:";
    public static String LABEL_PASSWORD_CONFIRM = "تکرار رمز عبور:";
    /********************************button****************************************/
    public static String BUTTON_CANCEL = "انصراف";
    public static String BUTTON_PRINT = "چاپ کن";
    public static String BUTTON_SET = "ثبت کن";
    public static String BUTTON_AT_MANAGER = "مدیریت نوع فعالیت ها";
    public static String BUTTON_CHANGE_BACKGROUND = "انتخاب عکس پس زمینه";
    public static String BUTTON_EXPORT = "انتقال جدول به Excel";
    public static String BUTTON_LOAD = "بارگذاری کن";
    public static String BUTTON_DEP_MANAGER = "مدیریت دانشکده ها";
    public static String BUTTON_QUERYFRAME = "ارائه اطلاعات";
    public static String BUTTON_EXIT = "خروج";
    public static String BUTTON_NEW_ACTIVITY = "ایجاد فعالیت جدید";
    public static String BUTTON_DELETE = "حذف کن";
    public static String BUTTON_DELETE_ACTIVITY = "فعالیت را حذف کن";
    public static String BUTTON_CHANGE = "تغییر بده";
    public static String BUTTON_ADD = "اضافه کن";
    public static String BUTTON_BACKUP_DATA = "ایجاد پرونده پشتیبان";
    public static String BUTTON_LOAD_DATA = "استفاده از پرونده پشتیبان";
    public static String BUTTON_BACK = "صفحه ی قبلی";
    public static String BUTTON_CONTINUE = "ادامه";
    public static String BUTTON_CHANGE_PASSWORD = "تغییر رمز عبور";
    /********************************message***************************************/
    public static String MESSAGE_UNABLE_TO_CONNECT = "امکان  برقراری ارتباط نیست";
    public static String MESSAGE_ERROR = "خطا";
    public static String MESSAGE_UNABLE_TO_CHANGE = "تغییرات انجام نپذیرفت.";
    public static String MESSAGE_CHANGES_APPLIED = "تغییرات با موفقیت ثبت شد.";
    public static String MESSAGE_WRONG_DATE = "در انتخاب تاریخ خطایی رخ داده است";
    public static String MESSAGE_WRONG_GRADE = "امتیاز به نادرستی وارد شده است.";
    public static String MESSAGE_WRONG_AC_ID = "شماره ی فعالیت صحیح نیست.";
    public static String MESSAGE_NECESSARY_FILE_MISSING = "یکی از فایل های ضروری در دسترس نیست.";
    public static String MESSAGE_NO_SUCH_AC_ID = "فعالیتی با این شماره وجود ندارد.";
    public static String MESSAGE_FILE_ALREADY_EXISTS = "یک فایل با همین نام وجود دارد. آیا مایل به پاک کردن فایل قبلی هستید؟";
    public static String MESSAGE_DB_CONNECTION_ERROR =
            "ارتباط با مرکزداده ها برقرار نیست.";
    public static String MESSAGE_CONFIRM_DELETE = "تعدادی فعالیت مربوطه در مرکز"
            + " داده ها وجود دارد. در صورت ادامه حذف خواهند شد. مایل به ادامه ه"
            + "ستید ؟";
    public static String MESSAGE_WRONG_AT_RATIO = "ضریب وارد شده معتبر نیست.";
    public static String MESSAGE_NO_AT_SELECTED = "هیچ نوع فعالیتی انتخاب نش"
            + "ده است.";
    public static String MESSAGE_SUBJECT_MISSING  = "عنوانی انتخاب نشده است.";
    public static String MESSAGE_NO_DEP_SELECTED = "هیچ دانشکده ای انتخاب نشده است.";
    public static String MESSAGE_PASSWORD_FIELD_WORNG = "رمز عبور وارد شده و یا تکرار آن اشتباه است";
    public static String MESSAGE_NO_AC_SELECTED = "فعالیتی انتخاب نشده است.";
    public static String MESSAGE_WRONG_FILE = "فایل انتخاب شده مناسب نیست.";
    public static String MESSAGE_WRONG_PASSWORD = "رمز عبور اشتباه است.";
    public static String MESSAGE_CONFIRM_DELETE_ACTIVITY = "آیا مایل به پاک کردن این فعالیت هستید؟";
    public static String WAMP_STARTER = "wampmanager.exe";
    public static String WAMP = "wamp";
}
