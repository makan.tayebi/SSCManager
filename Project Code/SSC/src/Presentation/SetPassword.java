/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation;

import Constants.Strings;
import DataStructure.Restricted;
import db_code.MySQLConnector;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import ssc.Main;

/**
 *
 * @author Emertat
 */
public class SetPassword extends JFrame implements ActionListener, Restricted {

    private JButton setPasswordButton;
    private JPasswordField passwordField, passwordConfirmField;
    private JLabel passwordLabel, passwordConfirmLabel;
    private String finalPass = "";
    private AbstractFrame caller;
    private int width = 250, height = 120;

    public SetPassword(AbstractFrame caller) {
        this.caller = caller;
        this.setSize(width, height);
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation((screenSize.width - width) / 2, (screenSize.height - height) / 2);
        this.setResizable(false);
        this.setLayout(null);
        this.setTitle(Strings.TITLE_SET_PASSWORD);
        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        this.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException ex) {
        } catch (InstantiationException ex) {
        } catch (IllegalAccessException ex) {
        } catch (UnsupportedLookAndFeelException ex) {
        }
        passwordLabel = new JLabel(Strings.LABEL_PASSWORD_COLON);
        passwordLabel.setLocation(170, 10);
        passwordLabel.setSize(passwordLabel.getPreferredSize());
        passwordLabel.setVisible(true);
        this.add(passwordLabel);

        passwordConfirmLabel = new JLabel(Strings.LABEL_PASSWORD_CONFIRM);
        passwordConfirmLabel.setLocation(170, 40);
        passwordConfirmLabel.setSize(passwordConfirmLabel.getPreferredSize());
        passwordConfirmLabel.setVisible(true);
        this.add(passwordConfirmLabel);

        setPasswordButton = new JButton(Strings.BUTTON_SET);
        setPasswordButton.setLocation(30, 65);
        setPasswordButton.setSize(setPasswordButton.getPreferredSize());
        setPasswordButton.setVisible(true);
        this.add(setPasswordButton);
        setPasswordButton.addActionListener(this);

        passwordField = new JPasswordField();
        passwordField.setLocation(10, 10);
        passwordField.setSize(150, 20);
        passwordField.setVisible(true);
        this.add(passwordField);

        passwordConfirmField = new JPasswordField();
        passwordConfirmField.setLocation(10, 40);
        passwordConfirmField.setSize(150, 20);
        passwordConfirmField.setVisible(true);
        this.add(passwordConfirmField);
        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(setPasswordButton)) {
            if (isFormValid()) {
                finalPass = new String(passwordField.getPassword());
                this.setVisible(false);
                if (caller != null) { // MainFrame has called me for CHANGING pass.
                    GetPassword gp = new GetPassword(this);
                    gp.setTitle(Strings.TITLE_OLD_PASSWORD);
                } else { // Installer has called me for SETTING pass for the FIRST TIME.
                    // just setting finalPass for the sake of Installer is ok. do nothing.
                }
                this.dispose();
            } else {
                Main.errorMessage(Strings.MESSAGE_PASSWORD_FIELD_WORNG, this);
            }
        }
    }

    private boolean isFormValid() {
        if (passwordField.getPassword().length != passwordConfirmField.getPassword().length
                || passwordField.getPassword().length == 0
                || passwordConfirmField.getPassword().length == 0) {
            return false;

        }
        for (int i = 0; i < passwordField.getPassword().length; i++) {
            if (passwordField.getPassword()[i] != passwordConfirmField.getPassword()[i]) {
                return false;
            }
        }
        return true;
    }

    public String getPassword() {
        return finalPass;
    }
    
    @Override
    public void passwordSet(String pass) {
        MySQLConnector db = new MySQLConnector();
        db.setPassword(pass); // old pass.
        db.changePassword(finalPass);
        caller.setVisible(true);
        caller.DB.setPassword(finalPass);
        this.dispose();
    }

    @Override
    public void cancelAction() {
        caller.setVisible(true);
        this.dispose();
    }
}
