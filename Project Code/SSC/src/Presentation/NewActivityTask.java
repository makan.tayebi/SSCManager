/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation;

import DataStructure.Activity;
import Constants.Strings;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;
import DataStructure.Activity.*;
import DataStructure.Date;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.event.KeyListener;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;
import ssc.Main;

/**
 *
 * @author Makann Tayebi
 */
public class NewActivityTask extends AbstractFrame implements ActionListener, WindowListener, KeyListener {

    JLabel dateLabel, gradeLabel, activityTypeLabel, departmentLabel, subjectLabel;
    JComboBox activityTypeComboBox;
    JComboBox departmentComboBox;
    JButton addActivityButton;
    /**
     *  0 for year, 1 for month and 2 for day.
     */
    JTextField datesTextField[];
    JTextField gradeTextField, subjectTextField;
    JPanel panel;
    private Activity ac;
    private int dep_IDs[], AT_IDs[];
    private String dep_Names[], AT_Names[];

    private void updateList() {
        dep_IDs = AbstractFrame.DB.getdep_IDs();
        dep_Names = AbstractFrame.DB.getdep_Names();
        AT_IDs = AbstractFrame.DB.getAT_IDs();
        AT_Names = AbstractFrame.DB.getAT_Names();

        departmentComboBox.removeAllItems();
        activityTypeComboBox.removeAllItems();

        for (int i = 0; i < dep_Names.length; i++) {
            departmentComboBox.addItem(dep_Names[i]);
        }
        for (int i = 0; i < AT_Names.length; i++) {
            activityTypeComboBox.addItem(AT_Names[i]);
        }
    }

    @Override
    public void setVisible(boolean b) {
        super.setVisible(b);
        if (b) {
            updateList();
        }
    }

    public NewActivityTask(AbstractFrame caller) {
        this.caller = caller;
        panel = new JPanel(null);
        panel.setSize(500, 400);
        panel.setLocation(windowX - 550, 50);
        panel.setVisible(true);
        panel.setBorder(new BevelBorder(WIDTH, Color.lightGray, Color.lightGray));
        this.setTitle(this.getTitle() + " - " + Strings.TITLE_NEWACTIVITYTASK);
        this.add(panel, 0);
        ac = new Activity();

        subjectLabel = new JLabel(Strings.LABEL_NAME);
        subjectLabel.setSize(subjectLabel.getPreferredSize());
        subjectLabel.setLocation(413, 260);
        panel.add(subjectLabel);
        subjectLabel.setVisible(true);

        subjectTextField = new JTextField();
        subjectTextField.setSize(450, 30);
        subjectTextField.setLocation(10, 300);
        subjectTextField.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        panel.add(subjectTextField, 0);
        subjectTextField.setVisible(true);

        activityTypeLabel = new JLabel(Strings.LABEL_AT);
        activityTypeLabel.setSize(activityTypeLabel.getPreferredSize());
        activityTypeLabel.setLocation(400, 80);
        activityTypeLabel.setVisible(true);
        activityTypeLabel.setOpaque(false);
        panel.add(activityTypeLabel);
        datesTextField = new JTextField[3];
        for (int i = 0; i < 3; i++) {
            datesTextField[i] = new JTextField();
            datesTextField[i].setVisible(true);
            datesTextField[i].addKeyListener(this);
            panel.add(datesTextField[i], 0);
        }
        datesTextField[0].setLocation(300, 210);
        datesTextField[1].setLocation(345, 210);
        datesTextField[2].setLocation(370, 210);
        datesTextField[0].setSize(40, 25);
        datesTextField[1].setSize(20, 25);
        datesTextField[2].setSize(20, 25);

        dateLabel = new JLabel(Strings.LABEL_DATE);
        dateLabel.setSize(dateLabel.getPreferredSize());
        dateLabel.setLocation(405, 210);
        dateLabel.setVisible(true);
        panel.add(dateLabel, 0);

        departmentLabel = new JLabel(Strings.LABEL_DEP);
        departmentLabel.setSize(departmentLabel.getPreferredSize());
        departmentLabel.setLocation(400, 40);
        departmentLabel.setVisible(true);
        panel.add(departmentLabel, 0);

        departmentComboBox = new JComboBox();
        DefaultListCellRenderer alignmenter = new DefaultListCellRenderer();
        alignmenter.setHorizontalAlignment(DefaultListCellRenderer.RIGHT);
        departmentComboBox.setRenderer(alignmenter);
        departmentComboBox.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        departmentComboBox.setLocation(50, 40);
        departmentComboBox.setSize(250, 25);
        departmentComboBox.setVisible(true);
        panel.add(departmentComboBox, 0);

        activityTypeComboBox = new JComboBox();
        activityTypeComboBox.setLocation(50, 80);
        activityTypeComboBox.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        activityTypeComboBox.setSize(250, 25);
        activityTypeComboBox.setVisible(true);
        activityTypeComboBox.setRenderer(alignmenter);
        panel.add(activityTypeComboBox);

        updateList();

        gradeLabel = new JLabel(Strings.LABEL_POINT);
        gradeLabel.setSize(gradeLabel.getPreferredSize());
        gradeLabel.setLocation(410, 170);
        gradeLabel.setVisible(true);
        panel.add(gradeLabel);

        gradeTextField = new JTextField();
        gradeTextField.setSize(70, 25);
        gradeTextField.setLocation(300, 170);
        gradeTextField.setVisible(true);
        panel.add(gradeTextField);
        gradeTextField.addKeyListener(this);

        addActivityButton = new JButton(Strings.BUTTON_ADD);
        addActivityButton.setSize(addActivityButton.getPreferredSize());
        addActivityButton.setLocation(10, 350);
        addActivityButton.setVisible(true);
        panel.add(addActivityButton);
        addActivityButton.addActionListener(this);

        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    }

    private void addActivity() {
        if (!AbstractFrame.DB.isConnected()) {
            // JOptionPane Error.
            Main.errorMessage(Strings.MESSAGE_DB_CONNECTION_ERROR, this);
            return;
        }
        if ("".equals(datesTextField[0].getText())
                || "".equals(datesTextField[1].getText())
                || "".equals(datesTextField[2].getText())) {
            Main.errorMessage(Strings.MESSAGE_WRONG_DATE, this);
            return;
        }
        try {
            Double.parseDouble(gradeTextField.getText());
            System.out.println(Double.parseDouble(gradeTextField.getText()));
        } catch (Exception ex) {
            Main.errorMessage(Strings.MESSAGE_WRONG_GRADE, this);
            return;
        }
        if (Integer.parseInt(datesTextField[2].getText()) < 1
                || Integer.parseInt(datesTextField[2].getText()) > 31
                || Integer.parseInt(datesTextField[1].getText()) > 12
                || Integer.parseInt(datesTextField[1].getText()) < 1
                || (Integer.parseInt(datesTextField[2].getText()) > 30
                && Integer.parseInt(datesTextField[1].getText()) < 7)
                || Integer.parseInt(datesTextField[0].getText()) < 1000
                || Integer.parseInt(datesTextField[0].getText()) > 10000) {
            Main.errorMessage(Strings.MESSAGE_WRONG_DATE, this);
            return;
        }
        try {
            ac.setDate(new Date(Integer.parseInt(datesTextField[0].getText()), Integer.parseInt(datesTextField[1].getText()), Integer.parseInt(datesTextField[2].getText())));
        } catch (Exception ex) {
            Main.errorMessage(Strings.MESSAGE_WRONG_DATE, this);
            return;
        }
        try {
            ac.setDepartmentID(dep_IDs[departmentComboBox.getSelectedIndex()]);
        } catch (Exception e) {
            Main.errorMessage(Strings.MESSAGE_NO_DEP_SELECTED, this);
            return;
        }
        try {
            ac.setTypeID(AT_IDs[activityTypeComboBox.getSelectedIndex()]);
        } catch (Exception e) {
            Main.errorMessage(Strings.MESSAGE_NO_AT_SELECTED, this);
            return; 
        }
        ac.setPoint(Integer.parseInt(gradeTextField.getText()));
        try {
            ac.setSubject(subjectTextField.getText());
            if (ac.getSubject().equals("") || ac.getSubject().equals(null)) {
                throw new Exception();
            }
        } catch (Exception ex) {
            Main.errorMessage(Strings.MESSAGE_SUBJECT_MISSING, this);
            return;
        }
        if (!AbstractFrame.DB.insertActivity(ac)) {
            Main.errorMessage(Strings.MESSAGE_UNABLE_TO_CHANGE, this);
            return;
        } else {
            Main.informMessage(Strings.MESSAGE_CHANGES_APPLIED, this);
            return;
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e);
        if (e.getSource() == addActivityButton) {
            addActivity();
        }
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
        caller.setVisible(true);
        this.setVisible(false);
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
        if (e.getKeyChar() > 57 || e.getKeyChar() < 48) {
            e.consume();
            return;
        }
        if(e.getSource().equals(datesTextField[1]) && datesTextField[1].getText().length() == 1){
            datesTextField[0].requestFocus();
        } else if(e.getSource().equals(datesTextField[2]) && datesTextField[2].getText().length() == 1){
            datesTextField[1].requestFocus();
        } else if(e.getSource().equals(datesTextField[0]) && datesTextField[0].getText().length() == 3){
            subjectTextField.requestFocus();

        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }
}
