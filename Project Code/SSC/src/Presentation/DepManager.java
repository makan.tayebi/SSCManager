/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation;

import Constants.Strings;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.SQLException;
import javax.swing.border.BevelBorder;
import ssc.Main;

/**
 *
 * @author Makan Tayebi
 */
public class DepManager extends AbstractFrame implements WindowListener, ActionListener {

    private javax.swing.JButton addButton;
    private javax.swing.JButton changeButton;
    private javax.swing.JButton deleteButton;
    private javax.swing.JComboBox depCombobox;
    private javax.swing.JTextField depNameTextField;
    private javax.swing.JLabel departmentNameLabel;
    private javax.swing.JLabel departmentSelectLabel;
    private javax.swing.JPanel panel;

    /**
     * Creates new form DepManager
     */
    public DepManager(AbstractFrame caller) {
        super();
        setCaller(caller);
        initComponents();
        this.setTitle(this.getTitle() + " - " + Strings.TITLE_DEPMANAGER);
        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    }
    String names[];
    int IDs[];

    private void update() {
        names = AbstractFrame.DB.getdep_Names();
        IDs = AbstractFrame.DB.getdep_IDs();
        depCombobox.removeActionListener(this);
        depCombobox.removeAllItems();
        for (int i = 0; i < names.length; i++) {
            depCombobox.addItem(names[i]);
        }
        depCombobox.addActionListener(this);
    }

    @Override
    public void setVisible(boolean b) {
        super.setVisible(b);
        if (b) {
            depNameTextField.setText(null);
            update();
        }
    }

    private void deleteButtonActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            boolean deleted = AbstractFrame.DB.deleteDepartment(IDs[depCombobox.getSelectedIndex()], false);
            if (deleted) {
                update();
                depCombobox.setSelectedIndex(0);
                loadSelected(IDs[depCombobox.getSelectedIndex()]);
                return;
            }
            int desicion = Main.confirmMessage(Strings.MESSAGE_CONFIRM_DELETE, this);
            if (desicion == 0) {
                AbstractFrame.DB.deleteDepartment(IDs[depCombobox.getSelectedIndex()], true);
                update();
                depCombobox.setSelectedIndex(0);
                loadSelected(IDs[depCombobox.getSelectedIndex()]);
                Main.informMessage(Strings.MESSAGE_CHANGES_APPLIED, this);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            Main.errorMessage(Strings.MESSAGE_DB_CONNECTION_ERROR, this);
        }
    }

    private void changeButtonActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            int selectedIndex = depCombobox.getSelectedIndex();
            if (AbstractFrame.DB.setDep_Name(IDs[selectedIndex], depNameTextField.getText())) {
                update();
                depCombobox.setSelectedIndex(selectedIndex);
                loadSelected(IDs[selectedIndex]);

                Main.informMessage(Strings.MESSAGE_CHANGES_APPLIED, this);
                return;
            }
        } catch (Exception e) {
        }
        Main.errorMessage(Strings.MESSAGE_UNABLE_TO_CHANGE, this);
    }

    private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {
        if (AbstractFrame.DB.addDepartment(depNameTextField.getText())) {
            update();
            depCombobox.setSelectedIndex(0);
            loadSelected(IDs[0]);
            Main.informMessage(Strings.MESSAGE_CHANGES_APPLIED, this);
            return;
        }
        Main.errorMessage(Strings.MESSAGE_UNABLE_TO_CHANGE, this);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
        getCaller().setVisible(true);
        this.setVisible(false);
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    private void initComponents() {

        panel = new javax.swing.JPanel();
        depCombobox = new javax.swing.JComboBox();
        departmentSelectLabel = new javax.swing.JLabel();
        deleteButton = new javax.swing.JButton();
        depNameTextField = new javax.swing.JTextField();
        changeButton = new javax.swing.JButton();
        addButton = new javax.swing.JButton();
        departmentNameLabel = new javax.swing.JLabel();

//        panel.setSize(500,500);
        this.add(panel, 0);


        depCombobox.setPreferredSize(new Dimension(200, 25));
        departmentSelectLabel.setText(Strings.LABEL_DEP);

        deleteButton.setText(Strings.BUTTON_DELETE);
        deleteButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteButtonActionPerformed(evt);
            }
        });

        changeButton.setText(Strings.BUTTON_CHANGE);
        changeButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                changeButtonActionPerformed(evt);
            }
        });

        addButton.setText(Strings.BUTTON_ADD);
        addButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonActionPerformed(evt);
            }
        });

        departmentNameLabel.setText(Strings.LABEL_NAME);
        depNameTextField.setPreferredSize(new Dimension(190, 25));
        depNameTextField.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);

        javax.swing.GroupLayout panelLayout = new javax.swing.GroupLayout(panel);
        panel.setLayout(panelLayout);
        panel.setBorder(new BevelBorder(BevelBorder.RAISED, Color.black, Color.black));
        panel.setLocation(windowX - panel.getSize().width - 20, 20);


        panelLayout.setHorizontalGroup(
                panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelLayout.createSequentialGroup().addContainerGap(38, Short.MAX_VALUE).addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING).addComponent(addButton).addComponent(changeButton).addComponent(deleteButton)).addGap(18, 18, 18).addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false).addGroup(panelLayout.createSequentialGroup().addComponent(depCombobox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE).addGap(19, 19, 19).addComponent(departmentSelectLabel)).addGroup(panelLayout.createSequentialGroup().addComponent(depNameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addComponent(departmentNameLabel))).addGap(6, 6, 6)));
        panelLayout.setVerticalGroup(
                panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(panelLayout.createSequentialGroup().addContainerGap().addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(departmentSelectLabel).addComponent(deleteButton).addComponent(depCombobox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)).addGap(11, 11, 11).addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(changeButton).addComponent(depNameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE).addComponent(departmentNameLabel)).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(addButton).addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup().addContainerGap(485, Short.MAX_VALUE).addComponent(panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE).addGap(20, 20, 20)));
        layout.setVerticalGroup(
                layout.createParallelGroup(
                javax.swing.GroupLayout.Alignment.LEADING).addGroup(
                layout.createSequentialGroup().addGap(22, 22, 22).addComponent(
                panel, javax.swing.GroupLayout.PREFERRED_SIZE,
                javax.swing.GroupLayout.DEFAULT_SIZE,
                javax.swing.GroupLayout.PREFERRED_SIZE).addContainerGap(193, Short.MAX_VALUE)));
    }

    private void loadSelected(int dep_ID) {
        depNameTextField.setText(AbstractFrame.DB.getDepName(dep_ID));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e);
        if (e.getSource().equals(depCombobox)) {
            loadSelected(IDs[depCombobox.getSelectedIndex()]);
        }
    }
}
