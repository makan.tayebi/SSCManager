/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation;

import Constants.Strings;
import DataStructure.Date;
import DataStructure.MyTable;
import DataStructure.OverallResult;
import DataStructure.Query;
import DataStructure.Result;
import DataStructure.TableContainer;
import DataStructure.Tuple_of_ACCount;
import DataStructure.Tuple_of_Ac;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Font;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import ssc.Main;

/**
 *
 * @author Makan Tayebi
 */
public class QueryFrame extends AbstractFrame implements ActionListener, WindowListener, KeyListener {

    JButton queryButton;
    JPanel multiActivityTypeSelectPanel, multiDepartmentSelectPanel,
            datePanel, singleDepartmentSelectPanel, singleActivityTypeSelectPanel;
    JComboBox addActivityTypeComboBox, deleteActivityTypeComboBox, addDepartmentComboBox,
            deleteDepartmentComboBox;
    JTextArea activityTypes_TextArea, departments_TextArea;
    JRadioButton overallRadioButton, detailedRadioButton, singleDepartmentRadioButton;
    JLabel activityLabel, departmentLabel, addDepartmentLabel
            , addActivityTypeLabel, deleteDepartmentLabel, deleteActivityLabel;
    JRadioButton all_ActivityTypesRadioButton, all_DepartmentsRadioButton
            , selection_DepartmentsRadioButton, selection_ActivityTypesRadioButton;
    ButtonGroup activitySelectionGroup, departmentSelectionGroup;
    ScrollPane activities_ScrollPane, departments_ScrollPane;
    ButtonGroup detailGroup;
    DefaultListCellRenderer alignmenter;
    JComboBox department_Single_Combo, activityType_Single_Combo;
    JLabel sdateLabel, fdateLabel;
    JTextField sDay, sMonth, sYear, fDay, fMonth, fYear;
    DeletableResultFrame detailView;
    OverallResultFrame overallView;
    private int dep_IDs[], dep_IDs_final[], dep_IDs_selection[];
    private int AT_IDs[], AT_IDs_final[], AT_IDs_selection[];
    private String dep_Names[], AT_Names[];

    private void updateList() {
        dep_IDs = AbstractFrame.DB.getdep_IDs();
        dep_Names = AbstractFrame.DB.getdep_Names();

        if (selection_DepartmentsRadioButton.isSelected()) {
            dep_IDs_final = dep_IDs_selection;
        } else {
            dep_IDs_final = dep_IDs;
        }

        AT_IDs = AbstractFrame.DB.getAT_IDs();
        AT_Names = AbstractFrame.DB.getAT_Names();
        if (selection_ActivityTypesRadioButton.isSelected()) {
            AT_IDs_final = AT_IDs_selection;
        } else {
            AT_IDs_final = AT_IDs;
        }
        // muting listeners, so we can easily modify comboBoxes.
        addDepartmentComboBox.removeActionListener(this);
        deleteDepartmentComboBox.removeActionListener(this);
        deleteActivityTypeComboBox.removeActionListener(this);
        addActivityTypeComboBox.removeActionListener(this);

        addDepartmentComboBox.removeAllItems();
        deleteDepartmentComboBox.removeAllItems();
        department_Single_Combo.removeAllItems();
        for (int i = 0; i < dep_Names.length; i++) {
            addDepartmentComboBox.addItem(dep_Names[i]);
            deleteDepartmentComboBox.addItem(dep_Names[i]);
            department_Single_Combo.addItem(dep_Names[i]);
        }

        deleteActivityTypeComboBox.removeAllItems();
        addActivityTypeComboBox.removeAllItems();
        activityType_Single_Combo.removeAllItems();
        for (int i = 0; i < AT_Names.length; i++) {
            deleteActivityTypeComboBox.addItem(AT_Names[i]);
            addActivityTypeComboBox.addItem(AT_Names[i]);
            activityType_Single_Combo.addItem(AT_Names[i]);
        }

        //  amplifying these listeners again.
        addDepartmentComboBox.addActionListener(this);
        deleteDepartmentComboBox.addActionListener(this);
        deleteActivityTypeComboBox.addActionListener(this);
        addActivityTypeComboBox.addActionListener(this);
    }

    @Override
    public void setVisible(boolean b) {
        super.setVisible(b);
        if (b) {
            updateList();
            actionPerformed(new ActionEvent(overallRadioButton, 0, null));
            overallRadioButton.setSelected(true);
        }
    }

    @Override
    public void dispose() {
        super.dispose();
        detailView.dispose();
        overallView.dispose();
    }

    public QueryFrame(AbstractFrame caller) {
        this.setTitle(this.getTitle() + " - " + Strings.TITLE_QUERYFRAME);
        setCaller(caller);
        dep_IDs_selection = new int[0];
        AT_IDs_selection = new int[0];

        alignmenter = new DefaultListCellRenderer();
        alignmenter.setHorizontalAlignment(DefaultListCellRenderer.RIGHT);

        initiateDatePanel();
        initiateMultiActivityTypeSelectPanel();
        initiateMultiDepartmentSelectPanel();
        initiateSingleDepartmentSelectPanel();
        initiateSingleActivityTypeSelectPanel();
        detailView = new DeletableResultFrame(this);
        overallView = new OverallResultFrame(this);
        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e);
        if (e.getSource().equals(detailedRadioButton)) {
            multiDepartmentSelectPanel.setVisible(false);
            multiActivityTypeSelectPanel.setVisible(false);
            singleActivityTypeSelectPanel.setVisible(true);
            singleDepartmentSelectPanel.setVisible(true);
        } else if (e.getSource().equals(overallRadioButton)) {
            singleDepartmentSelectPanel.setVisible(false);
            singleActivityTypeSelectPanel.setVisible(false);
            multiDepartmentSelectPanel.setVisible(true);
            multiActivityTypeSelectPanel.setVisible(true);
        } else if (e.getSource().equals(singleDepartmentRadioButton)) {
            multiDepartmentSelectPanel.setVisible(false);
            singleActivityTypeSelectPanel.setVisible(false);
            singleDepartmentSelectPanel.setVisible(true);
            multiActivityTypeSelectPanel.setVisible(true);
        }
        if (e.getSource().equals(all_ActivityTypesRadioButton)) {
            switch_ATs_to_all();
        } else if (e.getSource().equals(selection_ActivityTypesRadioButton)) {
            switch_ATs_to_selection();
        } else if (e.getSource().equals(all_DepartmentsRadioButton)) {
            switch_deps_to_all();
        } else if (e.getSource().equals(selection_DepartmentsRadioButton)) {
            switch_deps_to_selection();
        } else if (e.getSource().equals(addDepartmentComboBox)) {
            add_to_dep_selection(addDepartmentComboBox.getSelectedIndex());
        } else if (e.getSource().equals(deleteDepartmentComboBox)) {
            delete_from_dep_selection(deleteDepartmentComboBox.getSelectedIndex());
        } else if (e.getSource().equals(addActivityTypeComboBox)) {
            add_to_AT_selection(addActivityTypeComboBox.getSelectedIndex());
        } else if (e.getSource().equals(deleteActivityTypeComboBox)) {
            delete_from_AT_selection(deleteActivityTypeComboBox.getSelectedIndex());
        } else if (e.getSource().equals(queryButton)) {
            //Locking Buttons:
            overallRadioButton.setEnabled(false);
            detailedRadioButton.setEnabled(false);
            singleDepartmentRadioButton.setEnabled(false);
            queryButton.setEnabled(false);
            Query q = null;
            try {
                //making query
                q = new Query();
                q.setFinishLimit(new Date(Integer.parseInt(fYear.getText()), Integer.parseInt(fMonth.getText()), Integer.parseInt(fDay.getText())));
                q.setStartLimit(new Date(Integer.parseInt(sYear.getText()), Integer.parseInt(sMonth.getText()), Integer.parseInt(sDay.getText())));
            } catch (Exception ex) {
                Main.errorMessage(Strings.MESSAGE_WRONG_DATE, this);
                overallRadioButton.setEnabled(true);
                detailedRadioButton.setEnabled(true);
                singleDepartmentRadioButton.setEnabled(true);
                queryButton.setEnabled(true);
                return;
            }
            try {
                if (overallRadioButton.isSelected()) {
                    if (AT_IDs_final.length == 0 || dep_IDs_final.length == 0) {
                        Main.errorMessage(Strings.MESSAGE_ERROR, this);
                        overallRadioButton.setEnabled(true);
                        detailedRadioButton.setEnabled(true);
                        singleDepartmentRadioButton.setEnabled(true);
                        queryButton.setEnabled(true);
                        return;
                    }
                    q.setActivityTypeIDs(AT_IDs_final);
                    q.setDepartmentIDs(dep_IDs_final);
                    overallView.showData(overallTable(
                            DB.giveOverallAnswer(q), q));
                } else if (detailedRadioButton.isSelected()) {
                    int[] tempArray = new int[1];
                    tempArray[0] = AT_IDs[activityType_Single_Combo.getSelectedIndex()];
                    System.out.println("detected AT_ID: " + AT_IDs[activityType_Single_Combo.getSelectedIndex()]);
                    q.setActivityTypeIDs(tempArray);
                    tempArray = new int[1];
                    tempArray[0] = dep_IDs[department_Single_Combo.getSelectedIndex()];
                    q.setDepartmentIDs(tempArray);
                    detailView.showData(detailedTable(
                            DB.giveDetailedAnswer(q), q), q);
                } else if (singleDepartmentRadioButton.isSelected()) {
                    if (AT_IDs_final.length == 0) {
                        Main.errorMessage(Strings.MESSAGE_ERROR, this);
                        overallRadioButton.setEnabled(true);
                        detailedRadioButton.setEnabled(true);
                        singleDepartmentRadioButton.setEnabled(true);
                        queryButton.setEnabled(true);
                        return;
                    }
                    q.setActivityTypeIDs(AT_IDs_final);
                    int[] tempArray = new int[1];
                    tempArray[0] = dep_IDs[department_Single_Combo.getSelectedIndex()];
                    q.setDepartmentIDs(tempArray);
                    overallView.showData(singleDepartmentTable(
                            DB.giveOverallAnswer(q), q));
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            //Unlocking Buttons:
            overallRadioButton.setEnabled(true);
            detailedRadioButton.setEnabled(true);
            singleDepartmentRadioButton.setEnabled(true);
            queryButton.setEnabled(true);
        }
        System.gc();
    }

    private TableContainer overallTable(OverallResult r, Query q) {
        TableContainer tableContainer = new TableContainer(null);
        tableContainer.setSize(windowX - 350, windowY - 100);
        JLabel dateLabel = new JLabel(Strings.FROM_DATE_ + " : "
                + q.getStartLimit().toString_Reverse() + " , "
                + Strings.TO_DATE_ + " : "
                + q.getFinishLimit().toString_Reverse());
        dateLabel.setSize(dateLabel.getPreferredSize());
        dateLabel.setLocation(tableContainer.getWidth() - dateLabel.getWidth(), 10);
        tableContainer.add(dateLabel);
        MyTable table = new MyTable(q.getActivityTypeIDs().length + 1, q.getDepartmentIDs().length + 1 + 1);
        table.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.setHorizontalScrollBar(new JScrollBar(
                JScrollBar.HORIZONTAL));

        JScrollPane outerPane = new JScrollPane(scrollPane);
        tableContainer.add(outerPane);
        outerPane.setSize(tableContainer.getWidth(), tableContainer.getHeight() - 70);
        outerPane.setLocation(0, 35);

        r.reset();
        Tuple_of_ACCount t;
        for (int i = 0; i < r.size(); i++) {
            t = r.next();
            int j, k;
            for (j = 0; j < q.getActivityTypeIDs().length; j++) {
                if (t.getAt_ID() == q.getActivityTypeIDs()[j]) {
                    break;
                }
            }
            for (k = 0; k < q.getDepartmentIDs().length; k++) {
                if (t.getDep_ID() == q.getDepartmentIDs()[k]) {
                    break;
                }
            }
            table.getModel().setValueAt(
                    t.getNum() + "  /  " + t.getGradeSum(), j, k + 1);
        }
        {//filling "Sum" fields
            table.getModel().setValueAt(Strings.SUM, table.getRowCount() - 1, 0);
            table.getColumnModel().getColumn(table.getColumnCount() - 1).setHeaderValue(Strings.SUM);

            for (int i = 0; i < table.getRowCount() - 1; i++) {
                table.getModel().setValueAt(r.getCount_AT(
                        q.getActivityTypeIDs()[i]) + "  /  "
                        + r.getGradeSum_AT(q.getActivityTypeIDs()[i]), i, table.getColumnCount() - 1);
            }
            int totalCount = 0;
            double totalGrade = 0;
            for (int i = 1; i < table.getColumnCount() - 1; i++) {
                totalGrade += r.getGradeSum_Dep(q.getDepartmentIDs()[i - 1]);
                totalCount += r.getCount_Dep(q.getDepartmentIDs()[i - 1]);
                table.getModel().setValueAt(r.getCount_Dep(
                        q.getDepartmentIDs()[i - 1]) + "  /  "
                        + r.getGradeSum_Dep(q.getDepartmentIDs()[i - 1]), table.getRowCount() - 1, i);
            }
            table.getModel().setValueAt(totalCount + "  /  " + totalGrade,
                    table.getRowCount() - 1, table.getColumnCount() - 1);
        }

        for (int i = 0; i < table.getColumnCount(); i++) {
            TableColumnModel colModel = table.getColumnModel();
            TableColumn col = colModel.getColumn(i);
            col.setCellRenderer(new MyTableCellRenderer());
            table.getColumnModel().getColumn(i).setResizable(false);
            MultiLineHeaderRenderer headerRenderer = new MultiLineHeaderRenderer(
                    SwingConstants.RIGHT, SwingConstants.CENTER);
            col.setHeaderRenderer(headerRenderer);
        }
        table.getColumnModel().getColumn(0).setHeaderValue(Strings.ACTIVITY_TYPE);
        for (int i = 0; i < q.getDepartmentIDs().length; i++) {
            table.getColumnModel().getColumn(i + 1).setHeaderValue(
                    DB.getDepName(q.getDepartmentIDs()[i]));
        }
        for (int i = 0; i < table.getColumnCount(); i++) {
            if (table.getColumnModel().getColumn(i).getPreferredWidth() <
                    ((String) table.getColumnModel().getColumn(i).
                    getHeaderValue()).length() * 6) {
                table.getColumnModel().getColumn(i).setPreferredWidth(((String)
                        table.getColumnModel().getColumn(i).getHeaderValue()).
                        length() * 6);
            }
        }

        scrollPane.setSize(table.getPreferredSize().width, outerPane.getHeight()
                - 10);
        scrollPane.setPreferredSize(scrollPane.getSize());
        scrollPane.setLocation(outerPane.getWidth() / 2 - scrollPane.getWidth()
                / 2, 0);

        for (int i = 0; i < q.getActivityTypeIDs().length; i++) {
            table.getModel().setValueAt(DB.getAT_Name(q.getActivityTypeIDs()[i])
                    , i, 0);
        }

        tableContainer.setVisible(true);
        outerPane.setVisible(true);
        scrollPane.setVisible(true);
        dateLabel.setVisible(true);
        table.setVisible(true);
//        innerPanel.setVisible(true);

        tableContainer.setTable(table);
        return tableContainer;
    }

    private TableContainer singleDepartmentTable(OverallResult r, Query q) {

        TableContainer panel = new TableContainer(null);
        panel.setSize(500, 600);
        JLabel depNameLabel = new JLabel(Strings.DEPARTMENT + " : " + DB.
                getDepName(q.getActivityTypeIDs()[0]));
        JLabel dateLabel = new JLabel(Strings.FROM_DATE_ + " : " 
                + q.getStartLimit() + " , " + Strings.TO_DATE_ + " : "
                + q.getFinishLimit());
        depNameLabel.setVisible(true);
        dateLabel.setVisible(true);
        depNameLabel.setSize(depNameLabel.getPreferredSize());
        dateLabel.setSize(dateLabel.getPreferredSize());
        depNameLabel.setLocation(panel.getWidth() - depNameLabel.getWidth(), 10);
        dateLabel.setLocation(10, 10);
        panel.add(dateLabel);
        panel.add(depNameLabel);

        MyTable table = new MyTable(r.size() + 1, 3);
        table.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        for (int i = 0; i < 3; i++) {
            TableColumnModel colModel = table.getColumnModel();
            TableColumn col = colModel.getColumn(i);
            col.setCellRenderer(new MyTableCellRenderer());
            table.getColumnModel().getColumn(i).setResizable(false);
        }
        JScrollPane scrollPane = new JScrollPane(table);
        panel.add(scrollPane);
        scrollPane.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        table.getColumnModel().getColumn(0).setHeaderValue(Strings.ACTIVITY_TYPE);
        table.getColumnModel().getColumn(0).setPreferredWidth(300);
        table.getColumnModel().getColumn(1).setHeaderValue(Strings.NUMBER);
        table.getColumnModel().getColumn(1).setPreferredWidth(50);
        table.getColumnModel().getColumn(2).setHeaderValue(Strings.LABEL_POINT);
        table.getColumnModel().getColumn(2).setPreferredWidth(200);
        panel.add(scrollPane);

        table.getColumnModel().getColumn(0).setHeaderRenderer(
                new MultiLineHeaderRenderer(SwingConstants.RIGHT, SwingConstants.CENTER));
        table.getColumnModel().getColumn(1).setHeaderRenderer(
                new MultiLineHeaderRenderer(SwingConstants.RIGHT, SwingConstants.CENTER));
        table.getColumnModel().getColumn(2).setHeaderRenderer(
                new MultiLineHeaderRenderer(SwingConstants.RIGHT, SwingConstants.CENTER));

        table.setSize(table.getPreferredSize());
        scrollPane.setSize(table.getWidth(), 500);
        panel.setSize(table.getWidth(), 550);
        scrollPane.setLocation(panel.getWidth() / 2 - table.getWidth() / 2, 40);
        panel.setVisible(true);
        table.setVisible(true);
        scrollPane.setVisible(true);

        Tuple_of_ACCount t;
        for (int i = 0; i < r.size(); i++) {
            t = r.next();
            table.getModel().setValueAt(DB.getAT_Name(t.getAt_ID()), i, 0); //AT Name
            table.getModel().setValueAt(t.getNum(), i, 1); // AT number
            table.getModel().setValueAt(t.getGradeSum(), i, 2); // points
        }
        table.getModel().setValueAt(Strings.SUM, table.getRowCount() - 1, 0);
        table.getModel().setValueAt(r.getCount_Dep(q.getDepartmentIDs()[0]),
                table.getRowCount() - 1, 1);
        table.getModel().setValueAt(r.getGradeSum_Dep(q.getDepartmentIDs()[0]),
                table.getRowCount() - 1, 2);

        panel.setTable(table);
        return panel;
    }

    protected TableContainer detailedTable(Result r, Query q) {
        r.sortByDate();
        JLabel dep_name_Label = new JLabel(Strings.DEPARTMENT + " : "
                + DB.getDepName(q.getDepartmentIDs()[0]));
        JLabel ac_name_Label = new JLabel(Strings.ACTIVITY_TYPE + " : "
                + DB.getAT_Name(q.getActivityTypeIDs()[0]));

        MyTable table = new MyTable(r.size() + 1, 4);
        table.getColumnModel().getColumn(0).setHeaderValue(Strings.ACTIVITY_NAME);
        table.getColumnModel().getColumn(0).setPreferredWidth(500);

        table.getColumnModel().getColumn(1).setHeaderValue(Strings.LABEL_DATE);
        table.getColumnModel().getColumn(1).setPreferredWidth(100);

        table.getColumnModel().getColumn(2).setHeaderValue(Strings.LABEL_POINT);
        table.getColumnModel().getColumn(2).setPreferredWidth(100);

        table.getColumnModel().getColumn(3).setHeaderValue(Strings.AC_ID);
        table.getColumnModel().getColumn(3).setPreferredWidth(100);

        //alignment e cell ha tanzim shavad. Nam ha rastchin, addad ha vasatchin
        table.getColumnModel().getColumn(0).setHeaderRenderer(
                new MultiLineHeaderRenderer(SwingConstants.RIGHT, SwingConstants.CENTER));
        table.getColumnModel().getColumn(1).setHeaderRenderer(
                new MultiLineHeaderRenderer(SwingConstants.RIGHT, SwingConstants.CENTER));
        table.getColumnModel().getColumn(2).setHeaderRenderer(
                new MultiLineHeaderRenderer(SwingConstants.RIGHT, SwingConstants.CENTER));
        table.getColumnModel().getColumn(3).setHeaderRenderer(
                new MultiLineHeaderRenderer(SwingConstants.RIGHT, SwingConstants.CENTER));

        for (int i = 0; i < 4; i++) {
            TableColumnModel colModel = table.getColumnModel();
            TableColumn col = colModel.getColumn(i);
            col.setCellRenderer(new MyTableCellRenderer());
            table.getColumnModel().getColumn(i).setResizable(false);
        }

        r.reset();
        Tuple_of_Ac t;
        String floatingPoint[];
        String finalRating;
        for (int i = 0; i < r.size(); i++) {

            t = r.next();

            table.setRowHeight(i, 30);
            table.getModel().setValueAt(t.getAc_name(), i, 0);
            table.getModel().setValueAt(t.getDate().toString(), i, 1);
            floatingPoint = ("" + ((double) t.getPoint() * DB.getATRatio(t.getAt_id()))).split("\\.");
            // we try to maintain the length of the double value if it is too long.
            finalRating = floatingPoint[0];
            try{
                finalRating += "." + floatingPoint[1].substring(0, 3);
            }catch(Exception ex) {
                // rate was a natural number.
            }
            table.getModel().setValueAt(finalRating, i, 2);
            table.getModel().setValueAt(t.getAc_id(), i, 3);
        }
        table.getModel().setValueAt(Strings.SUM, table.getRowCount() - 1, 0);
        table.getModel().setValueAt(" - ", table.getRowCount() - 1, 1);
        double sum = r.sum().getPoint() * DB.getATRatio(
                q.getActivityTypeIDs()[0]);
        floatingPoint = ("" + sum).split("\\.");
        finalRating= floatingPoint[0];
            try{
                finalRating += "." + floatingPoint[1].substring(0, 3);
            }catch(Exception ex) {
                // rate was a natural number.
            }        
        table.getModel().setValueAt(finalRating, table.getRowCount() - 1, 2);
        table.getModel().setValueAt(" - ", table.getRowCount() - 1, 3);
        table.setSize(table.getPreferredSize());
        table.getTableHeader().setBackground(Color.CYAN);

        TableContainer panel = new TableContainer(null);
        JScrollPane scroll = new JScrollPane(table);
        panel.setSize(table.getWidth(), 400);
        scroll.setLocation(0, 60);
        scroll.setSize(panel.getWidth(), panel.getHeight() - scroll.getY());
        panel.add(dep_name_Label);
        dep_name_Label.setSize(dep_name_Label.getPreferredSize());
        dep_name_Label.setLocation(panel.getWidth() - (dep_name_Label.getWidth() + 10), 10);
        dep_name_Label.setVisible(true);

        panel.add(ac_name_Label);
        ac_name_Label.setSize(ac_name_Label.getPreferredSize());
        ac_name_Label.setLocation(panel.getWidth() - (10 + ac_name_Label.getWidth()), 35);
        ac_name_Label.setVisible(true);

        panel.setVisible(true);
        scroll.setVisible(true);
        table.setVisible(true);
        panel.add(scroll);

        panel.setTable(table);
        return panel;
    }

    private void delete_from_AT_selection(int itemNumber) {
        if (AT_IDs_selection.length == 0) {
            return;
        }
        int temp[] = new int[AT_IDs_selection.length - 1], k;
        for (k = 0; k < AT_IDs_selection.length; k++) {
            if (AT_IDs_selection[k] == AT_IDs[itemNumber]) {
                break;
            }
            temp[k] = AT_IDs_selection[k];
        }
        for (int i = k; i < temp.length; i++) {
            temp[i] = AT_IDs_selection[i + 1];

        }
        AT_IDs_selection = temp;
//        VISUAL OPERATION:
        activityTypes_TextArea.setText("");
        for (int i = 0; i < AT_IDs_selection.length; i++) {
            int tempInt = -1;
            for (int j = 0; j < AT_IDs.length; j++) {
                if (AT_IDs[j] == AT_IDs_selection[i]) {
                    tempInt = j;
                    break;
                }
            }
            activityTypes_TextArea.setText(activityTypes_TextArea.getText()
                    + AT_Names[tempInt] + "\n");
        }
        AT_IDs_final = AT_IDs_selection;
    }

    private void switch_ATs_to_all() {
//        VISUAL OPERARIONS:
        addActivityTypeComboBox.setEnabled(false);
        deleteActivityTypeComboBox.setEnabled(false);
        addActivityTypeLabel.setEnabled(false);
        deleteActivityLabel.setEnabled(false);
//        BACKGROUND OPERATIONS:
        AT_IDs_final = AT_IDs;
    }

    private void switch_ATs_to_selection() {
//        VISUAL OPERATIONS:
        addActivityTypeComboBox.setEnabled(true);
        deleteActivityTypeComboBox.setEnabled(true);
        addActivityTypeLabel.setEnabled(true);
        deleteActivityLabel.setEnabled(true);
//            BACKGROUND OPERATIONS
        AT_IDs_final = AT_IDs_selection;
    }

    private void add_to_AT_selection(int itemNumber) {
//        BACKGROUND OPERATIONS:
//        search into currently selected AT_IDs:
        for (int i = 0; i < AT_IDs_selection.length; i++) {
            if (AT_IDs_selection[i] == AT_IDs[itemNumber]) {
                return;
            }
        }
//        now we assume this AT is not already selected.
//        refreshing AT selected list:
        int temp[] = new int[AT_IDs_selection.length + 1];
        for (int i = 0; i < AT_IDs_selection.length; i++) {
            temp[i] = AT_IDs_selection[i];
        }
        temp[temp.length - 1] = AT_IDs[itemNumber];
        AT_IDs_selection = temp;
//        VISUAL OPERATION:
        activityTypes_TextArea.setText("");
        for (int i = 0; i < AT_IDs_selection.length; i++) {
            int tempInt = -1;
            for (int j = 0; j < AT_IDs.length; j++) {
                if (AT_IDs[j] == AT_IDs_selection[i]) {
                    tempInt = j;
                    break;
                }
            }
            activityTypes_TextArea.setText(activityTypes_TextArea.getText()
                    + AT_Names[tempInt]
                    + "\n");
        }
        AT_IDs_final = AT_IDs_selection;
    }

    private void add_to_dep_selection(int itemNumber) {
//        BACKGROUND OPERATIONS:
//        search into currently selected dep_IDs:
        for (int i = 0; i < dep_IDs_selection.length; i++) {
            if (dep_IDs_selection[i] == dep_IDs[itemNumber]) {
                return;
            }
        }
//        now we assume this Depis not already selected.
//        refreshing dep selected list:
        int temp[] = new int[dep_IDs_selection.length + 1];
        for (int i = 0; i < dep_IDs_selection.length; i++) {
            temp[i] = dep_IDs_selection[i];
        }
        temp[temp.length - 1] = dep_IDs[itemNumber];
        dep_IDs_selection = temp;
//        VISUAL OPERATION:
        departments_TextArea.setText("");
        for (int i = 0; i < dep_IDs_selection.length; i++) {
            int tempInt = -1;
            for (int j = 0; j < dep_IDs.length; j++) {
                if (dep_IDs[j] == dep_IDs_selection[i]) {
                    tempInt = j;
                    break;
                }
            }
            departments_TextArea.setText(departments_TextArea.getText()
                    + dep_Names[tempInt]
                    + "\n");
        }
        dep_IDs_final = dep_IDs_selection;
    }

    private void delete_from_dep_selection(int itemNumber) {
//        BACKGROUND OPERATIONS:
        if (dep_IDs_selection.length == 0) {
            return;
        }
        int temp[] = new int[dep_IDs_selection.length - 1], k;
        for (k = 0; k < dep_IDs_selection.length; k++) {
            if (dep_IDs_selection[k] == dep_IDs[itemNumber]) {
                break;
            }
            temp[k] = dep_IDs_selection[k];
        }
        for (int i = k; i < temp.length; i++) {
            temp[i] = dep_IDs_selection[i + 1];

        }
        dep_IDs_selection = temp;
//        VISUAL OPERATION:
        departments_TextArea.setText("");
        for (int i = 0; i < dep_IDs_selection.length; i++) {
            int tempInt = -1;
            for (int j = 0; j < dep_IDs.length; j++) {
                if (dep_IDs[j] == dep_IDs_selection[i]) {
                    tempInt = j;
                    break;
                }
            }
            departments_TextArea.setText(departments_TextArea.getText() + dep_Names[tempInt] + "\n");
        }
        dep_IDs_final = dep_IDs_selection;
    }

    private void switch_deps_to_selection() {
//        VISUAL OPERATIONS:
        addDepartmentComboBox.setEnabled(true);
        deleteDepartmentComboBox.setEnabled(true);
        addDepartmentLabel.setEnabled(true);
        deleteDepartmentLabel.setEnabled(true);
//            BACKGROUND OPERATIONS
        dep_IDs_final = dep_IDs_selection;
    }

    private void switch_deps_to_all() {
//        VISUAL OPERARIONS:
        addDepartmentComboBox.setEnabled(false);
        deleteDepartmentComboBox.setEnabled(false);
        addDepartmentLabel.setEnabled(false);
        deleteDepartmentLabel.setEnabled(false);
//        BACKGROUND OPERATIONS:
        dep_IDs_final = dep_IDs;
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
        getCaller().setVisible(true);
        this.setVisible(false);
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
        if ((e.getKeyChar() > 57 || e.getKeyChar() < 48)) {
            e.consume();
        }
        if(e.getSource().equals(sDay) && sDay.getText().length() == 1) {
            sMonth.requestFocus();
        } else if(e.getSource().equals(sMonth) && sMonth.getText().length() == 1) {
            sYear.requestFocus();
        } else if(e.getSource().equals(sYear) && sYear.getText().length() == 3) {
            fDay.requestFocus();
        } else if(e.getSource().equals(fDay) && fDay.getText().length() == 1) {
            fMonth.requestFocus();
        } else if(e.getSource().equals(fMonth) && fMonth.getText().length() == 1) {
            fYear.requestFocus();
        } else if(e.getSource().equals(fYear) && fYear.getText().length() == 3) {
            queryButton.requestFocus();
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    private void initiateSingleDepartmentSelectPanel() {
        singleDepartmentSelectPanel = new JPanel();
        this.add(singleDepartmentSelectPanel, 0);
        singleDepartmentSelectPanel.setSize(400, 100);
        singleDepartmentSelectPanel.setLocation(windowX
                - multiActivityTypeSelectPanel.getWidth()
                - datePanel.getWidth()
                - singleDepartmentSelectPanel.getWidth() - 30, 10);
        singleDepartmentSelectPanel.setVisible(true);
        singleDepartmentSelectPanel.setBorder(new BevelBorder(BevelBorder.RAISED
                , Color.black, Color.black));

        departmentLabel = new JLabel(Strings.LABEL_DEP);
        departmentLabel.setSize(departmentLabel.getPreferredSize());
        departmentLabel.setLocation(305, 10);
        departmentLabel.setVisible(true);
        singleDepartmentSelectPanel.add(departmentLabel);

        department_Single_Combo = new JComboBox();
        department_Single_Combo.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        department_Single_Combo.setSize(300, 20);
        department_Single_Combo.setLocation(10, 60);
        department_Single_Combo.setVisible(true);
        singleDepartmentSelectPanel.add(department_Single_Combo, 0);
        department_Single_Combo.addActionListener(this);
        department_Single_Combo.setRenderer(alignmenter);
    }

    private void initiateSingleActivityTypeSelectPanel() {
        singleActivityTypeSelectPanel = new JPanel();
        this.add(singleActivityTypeSelectPanel, 0);
        singleActivityTypeSelectPanel.setSize(400, 100);
        singleActivityTypeSelectPanel.setLocation(windowX 
                - multiActivityTypeSelectPanel.getWidth()
                - datePanel.getWidth()
                - singleActivityTypeSelectPanel.getWidth()
                - 30, singleDepartmentSelectPanel.getHeight() + 20);
        singleActivityTypeSelectPanel.setVisible(true);
        singleActivityTypeSelectPanel.setBorder(
                new BevelBorder(BevelBorder.RAISED, Color.black, Color.black));

        activityLabel = new JLabel(Strings.LABEL_AT);
        activityLabel.setSize(activityLabel.getPreferredSize());
        activityLabel.setLocation(305, 10);
        activityLabel.setVisible(true);
        singleActivityTypeSelectPanel.add(activityLabel);

        activityType_Single_Combo = new JComboBox();
        activityType_Single_Combo.setComponentOrientation(
                ComponentOrientation.RIGHT_TO_LEFT);
        activityType_Single_Combo.setSize(300, 20);
        activityType_Single_Combo.setLocation(10, 60);
        activityType_Single_Combo.setVisible(true);
        singleActivityTypeSelectPanel.add(activityType_Single_Combo, 0);
        activityType_Single_Combo.addActionListener(this);
        activityType_Single_Combo.setRenderer(alignmenter);

    }

    private void initiateMultiDepartmentSelectPanel() {
        multiDepartmentSelectPanel = new JPanel(null);
        this.add(multiDepartmentSelectPanel, 0);
        multiDepartmentSelectPanel.setSize(400, 280);
        multiDepartmentSelectPanel.setLocation(windowX - datePanel.getWidth()
                - multiActivityTypeSelectPanel.getWidth() - multiDepartmentSelectPanel.getWidth() - 30, 10);
        multiDepartmentSelectPanel.setVisible(true);
        multiDepartmentSelectPanel.setBorder(new BevelBorder(BevelBorder.RAISED, Color.black, Color.black));
        departmentLabel = new JLabel(Strings.LABEL_DEP);
        departmentLabel.setSize(departmentLabel.getPreferredSize());
        departmentLabel.setLocation(305, 10);
        departmentLabel.setVisible(true);
        multiDepartmentSelectPanel.add(departmentLabel, 0);

        all_DepartmentsRadioButton = new JRadioButton(Strings.ALL);
        all_DepartmentsRadioButton.setSize(all_DepartmentsRadioButton.getPreferredSize());
        all_DepartmentsRadioButton.setLocation(200, 10);
        all_DepartmentsRadioButton.setVisible(true);
        multiDepartmentSelectPanel.add(all_DepartmentsRadioButton, 0);
        all_DepartmentsRadioButton.addActionListener(this);

        selection_DepartmentsRadioButton = new JRadioButton(Strings.SELECTION);
        selection_DepartmentsRadioButton.setSize(selection_ActivityTypesRadioButton.getPreferredSize());
        selection_DepartmentsRadioButton.setLocation(200, 30);
        selection_DepartmentsRadioButton.setVisible(true);
        multiDepartmentSelectPanel.add(selection_DepartmentsRadioButton, 0);
        selection_DepartmentsRadioButton.addActionListener(this);
        selection_DepartmentsRadioButton.setSelected(true);

        departmentSelectionGroup = new ButtonGroup();
        departmentSelectionGroup.add(all_DepartmentsRadioButton);
        departmentSelectionGroup.add(selection_DepartmentsRadioButton);
        addDepartmentLabel = new JLabel(Strings.BUTTON_ADD);
        addDepartmentLabel.setSize(addDepartmentLabel.getPreferredSize());
        addDepartmentLabel.setLocation(320, 60);
        addDepartmentLabel.setVisible(true);
        multiDepartmentSelectPanel.add(addDepartmentLabel, 0);

        deleteDepartmentLabel = new JLabel(Strings.BUTTON_DELETE);
        deleteDepartmentLabel.setSize(deleteDepartmentLabel.getPreferredSize());
        deleteDepartmentLabel.setLocation(320, 90);
        multiDepartmentSelectPanel.add(deleteDepartmentLabel, 0);
        deleteDepartmentLabel.setVisible(true);

        addDepartmentComboBox = new JComboBox();
        addDepartmentComboBox.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        addDepartmentComboBox.setSize(300, 20);
        addDepartmentComboBox.setLocation(10, 60);
        addDepartmentComboBox.setVisible(true);
        multiDepartmentSelectPanel.add(addDepartmentComboBox, 0);
        addDepartmentComboBox.addActionListener(this);

        deleteDepartmentComboBox = new JComboBox();
        deleteDepartmentComboBox.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        deleteDepartmentComboBox.setSize(300, 20);
        deleteDepartmentComboBox.setLocation(10, 85);
        deleteDepartmentComboBox.setVisible(true);
        multiDepartmentSelectPanel.add(deleteDepartmentComboBox, 0);
        deleteDepartmentComboBox.addActionListener(this);

        departments_ScrollPane = new ScrollPane();
        departments_ScrollPane.setSize(300, 160);
        departments_ScrollPane.setLocation(10, 110);
        multiDepartmentSelectPanel.add(departments_ScrollPane, 0);
        departments_ScrollPane.setVisible(true);

        departments_TextArea = new JTextArea();
        departments_ScrollPane.add(departments_TextArea, 0);
        departments_TextArea.setVisible(true);
        departments_TextArea.setEditable(false);
        departments_TextArea.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        try {
            departments_TextArea.setFont(Font.createFont(
                    Font.TRUETYPE_FONT, new java.io.File(Strings.Font_BNazanin)).
                    deriveFont(Font.BOLD, (float) (16)));
        } catch (Exception ex) {
        }

        addDepartmentComboBox.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        deleteDepartmentComboBox.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        addDepartmentComboBox.setRenderer(alignmenter);
        deleteDepartmentComboBox.setRenderer(alignmenter);
    }

    private void initiateDatePanel() {
        datePanel = new JPanel(null);
        this.add(datePanel, 0);
        datePanel.setSize(200, 280);
        datePanel.setLocation(windowX - 10 - datePanel.getWidth(), 10);
        datePanel.setBorder(new BevelBorder(BevelBorder.RAISED, Color.black, Color.black));
        datePanel.setVisible(true);

        sdateLabel = new JLabel(Strings.FROM_DATE_);
        sdateLabel.setLocation(130, 140);
        sdateLabel.setSize(sdateLabel.getPreferredSize());
        datePanel.add(sdateLabel, 0);
        sdateLabel.setVisible(true);

        fdateLabel = new JLabel(Strings.TO_DATE_);
        fdateLabel.setLocation(130, 190);
        fdateLabel.setSize(fdateLabel.getPreferredSize());
        datePanel.add(fdateLabel, 0);
        fdateLabel.setVisible(true);

        sDay = new JTextField();
        sDay.addKeyListener(this);
        datePanel.add(sDay, 0);
        sDay.setSize(20, 20);
        sDay.setLocation(90, 140);
        sDay.setVisible(true);

        sMonth = new JTextField();
        sMonth.setSize(20, 20);
        sMonth.setLocation(60, 140);
        datePanel.add(sMonth, 0);
        sMonth.addKeyListener(this);
        sMonth.setVisible(true);

        sYear = new JTextField();
        sYear.setSize(40, 20);
        sYear.setLocation(10, 140);
        datePanel.add(sYear, 0);
        sYear.addKeyListener(this);
        sYear.setText("13");
        sYear.setVisible(true);

        fDay = new JTextField();
        fDay.setSize(20, 20);
        fDay.setLocation(90, 190);
        datePanel.add(fDay, 0);
        fDay.addKeyListener(this);
        fDay.setVisible(true);

        fMonth = new JTextField();
        fMonth.setSize(20, 20);
        fMonth.setLocation(60, 190);
        datePanel.add(fMonth, 0);
        fMonth.addKeyListener(this);
        fMonth.setVisible(true);

        fYear = new JTextField();
        fYear.setSize(40, 20);
        fYear.setLocation(10, 190);
        datePanel.add(fYear, 0);
        fYear.addKeyListener(this);
        fYear.setVisible(true);

        queryButton = new JButton(Strings.SUBMIT_BUTTON);
        queryButton.setSize(queryButton.getPreferredSize());
        queryButton.setLocation(5, 250);
        datePanel.add(queryButton, 0);
        queryButton.addActionListener(this);
        queryButton.setVisible(true);

        detailedRadioButton = new JRadioButton(Strings.DETAIL);
        overallRadioButton = new JRadioButton(Strings.OVERALL_LABEL);
        singleDepartmentRadioButton = new JRadioButton(Strings.LABEL_SINGLE_DEPARTMENT);

        detailedRadioButton.setSize(detailedRadioButton.getPreferredSize());
        overallRadioButton.setSize(110, detailedRadioButton.getPreferredSize().height);
        singleDepartmentRadioButton.setSize(130, singleDepartmentRadioButton.getPreferredSize().height);

        overallRadioButton.setLocation(10, 10);
        detailedRadioButton.setLocation(10, 30);
        singleDepartmentRadioButton.setLocation(10, 50);

        datePanel.add(detailedRadioButton, 0);
        datePanel.add(overallRadioButton, 0);
        datePanel.add(singleDepartmentRadioButton, 0);

        detailedRadioButton.addActionListener(this);
        overallRadioButton.addActionListener(this);
        singleDepartmentRadioButton.addActionListener(this);

        overallRadioButton.setVisible(true);
        detailedRadioButton.setVisible(true);
        singleDepartmentRadioButton.setVisible(true);
        detailGroup = new ButtonGroup();
        detailGroup.add(detailedRadioButton);
        detailGroup.add(overallRadioButton);
        detailGroup.add(singleDepartmentRadioButton);
    }

    private void initiateMultiActivityTypeSelectPanel() {
        multiActivityTypeSelectPanel = new JPanel(null);
        this.add(multiActivityTypeSelectPanel, 0);
        multiActivityTypeSelectPanel.setSize(400, 280);
        multiActivityTypeSelectPanel.setLocation(windowX - datePanel.getWidth()
                - multiActivityTypeSelectPanel.getWidth() - 20, 10);
        multiActivityTypeSelectPanel.setVisible(true);
        multiActivityTypeSelectPanel.setBorder(new BevelBorder(BevelBorder.RAISED, Color.black, Color.black));

        activityLabel = new JLabel(Strings.LABEL_AT);
        activityLabel.setSize(activityLabel.getPreferredSize());
        activityLabel.setLocation(305, 10);
        activityLabel.setVisible(true);
        multiActivityTypeSelectPanel.add(activityLabel, 0);

        all_ActivityTypesRadioButton = new JRadioButton(Strings.ALL);
        all_ActivityTypesRadioButton.setSize(all_ActivityTypesRadioButton.getPreferredSize());
        all_ActivityTypesRadioButton.setLocation(200, 10);
        all_ActivityTypesRadioButton.setVisible(true);
        multiActivityTypeSelectPanel.add(all_ActivityTypesRadioButton, 0);
        all_ActivityTypesRadioButton.addActionListener(this);

        selection_ActivityTypesRadioButton = new JRadioButton(Strings.SELECTION);
        selection_ActivityTypesRadioButton.setSize(selection_ActivityTypesRadioButton.getPreferredSize());
        selection_ActivityTypesRadioButton.setLocation(200, 30);
        selection_ActivityTypesRadioButton.setVisible(true);
        multiActivityTypeSelectPanel.add(selection_ActivityTypesRadioButton, 0);
        selection_ActivityTypesRadioButton.addActionListener(this);
        selection_ActivityTypesRadioButton.setSelected(true);


        activitySelectionGroup = new ButtonGroup();
        activitySelectionGroup.add(all_ActivityTypesRadioButton);
        activitySelectionGroup.add(selection_ActivityTypesRadioButton);

        addActivityTypeLabel = new JLabel(Strings.BUTTON_ADD);
        addActivityTypeLabel.setSize(addActivityTypeLabel.getPreferredSize());
        addActivityTypeLabel.setLocation(320, 60);
        addActivityTypeLabel.setVisible(true);
        multiActivityTypeSelectPanel.add(addActivityTypeLabel, 0);

        deleteActivityLabel = new JLabel(Strings.BUTTON_DELETE);
        deleteActivityLabel.setSize(deleteActivityLabel.getPreferredSize());
        deleteActivityLabel.setLocation(320, 90);
        multiActivityTypeSelectPanel.add(deleteActivityLabel, 0);
        deleteActivityLabel.setVisible(true);

        addActivityTypeComboBox = new JComboBox();
        addActivityTypeComboBox.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        addActivityTypeComboBox.setSize(300, 20);
        addActivityTypeComboBox.setLocation(10, 60);
        addActivityTypeComboBox.setVisible(true);
        multiActivityTypeSelectPanel.add(addActivityTypeComboBox, 0);
        addActivityTypeComboBox.addActionListener(this);
        deleteActivityTypeComboBox = new JComboBox();
        deleteActivityTypeComboBox.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        deleteActivityTypeComboBox.setSize(300, 20);
        deleteActivityTypeComboBox.setLocation(10, 85);
        deleteActivityTypeComboBox.setVisible(true);
        multiActivityTypeSelectPanel.add(deleteActivityTypeComboBox, 0);

        deleteActivityTypeComboBox.addActionListener(this);
        addActivityTypeComboBox.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        deleteActivityTypeComboBox.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        addActivityTypeComboBox.setRenderer(alignmenter);
        deleteActivityTypeComboBox.setRenderer(alignmenter);
        activities_ScrollPane = new ScrollPane();
        activities_ScrollPane.setSize(300, 160);
        activities_ScrollPane.setLocation(10, 110);
        multiActivityTypeSelectPanel.add(activities_ScrollPane);
        activities_ScrollPane.setVisible(true);

        activityTypes_TextArea = new JTextArea();
        activities_ScrollPane.add(activityTypes_TextArea);
        activityTypes_TextArea.setVisible(true);
        activityTypes_TextArea.setEditable(false);
        activityTypes_TextArea.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        try {
            activityTypes_TextArea.setFont(Font.createFont(
                    Font.TRUETYPE_FONT, new java.io.File(Strings.Font_BNazanin))
                    .deriveFont(Font.BOLD, (float) (16)));
        } catch (Exception ex) {
        }
    }
}

class MyTableCellRenderer implements TableCellRenderer {

    JScrollPane scrollPane;
    JTextArea textArea;

    public MyTableCellRenderer() {
        textArea = new JTextArea();
        scrollPane = new JScrollPane(textArea);
        textArea.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        textArea.setFont(new Font("tahoma", Font.PLAIN, 12));
        textArea.setWrapStyleWord(true);
        textArea.setLineWrap(true);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value,
            boolean isSelected, boolean hasFocus, int row, int column) {
        //setting cell text
        textArea.setText(value.toString());
        //visual enhancements:
        textArea.setForeground(Color.BLACK);
        if (isSelected) {
            textArea.setBackground(Color.CYAN);
        } else if (hasFocus) {
            textArea.setBackground(Color.YELLOW);
        } else if (row % 2 == 0) {
            textArea.setBackground(Color.LIGHT_GRAY);
        } else {
            textArea.setBackground(Color.white);
        }
        //adjust row height for multi-lines
        if (column == 0) {
            table.setRowHeight(row, 20 * numberOfLines(value.toString(),
                    (table.getColumnModel().getColumn(0).getWidth() / 7)));
        }
        return scrollPane;
    }

    private int numberOfLines(String text, int maxCharsInLine) {
        int result = 1;
        int charsInCurrentLine = 0;
        for (String token : text.split(" ")) {// a word is bigger than line width.
            if (token.length() >= maxCharsInLine) {
                result += Math.floor((double) token.length() / maxCharsInLine);
                charsInCurrentLine = 1 + (token.length() % maxCharsInLine);
            } else {
                if (token.length() <= maxCharsInLine - charsInCurrentLine) {
                    //new word in same line.
                    charsInCurrentLine += token.length() + 1;
                } else {
                    charsInCurrentLine = token.length() + 1; //new word in new line
                    result++;
                }
            }
        }
        return result;
    }
}
