/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation;

import Constants.Strings;
import db_code.MySQLConnector;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;
import ssc.Main;

/**
 *
 * @author Makan Tayebi
 */
public class MainFrame extends AbstractFrame implements ActionListener, WindowListener {

    JButton newActivityButton, queryButton, atManage_trigger, depManage_trigger,
            wallPaperChooserButton, exitButton, backupButton, loadButton,
            changePasswordButton;
    ATManager atManager;
    NewActivityTask newActivityTask;
    JLabel author;
    QueryFrame queryFrame;
    DepManager depManager;
    JPanel menu;

    public MainFrame(MySQLConnector db) {
        DB = db;
        this.setBackground();
        queryFrame = new QueryFrame(this);
        queryFrame.setBackground();
        newActivityTask = new NewActivityTask(this);
        newActivityTask.setBackground();
        atManager = new ATManager(this);
        atManager.setBackground();
        depManager = new DepManager(this);
        depManager.setBackground();
        this.setTitle(this.getTitle() + " - " + Strings.TITLE_MAINFRAME);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

        author = new JLabel(Strings.LABEL_COPY_RIGHT);
        author.setSize(author.getPreferredSize());
        author.setLocation(windowX - author.getPreferredSize().width - 20,
                windowY - author.getPreferredSize().height - 30);
        author.setOpaque(true);
        author.setVisible(true);

        this.add(author, 0);
        initiateButtonsPanel();
        backButton.setVisible(false);
        this.repaint();
        this.setVisible(true);
    }

    private void initiateButtonsPanel() {
        changePasswordButton = new JButton(Strings.BUTTON_CHANGE_PASSWORD);
        changePasswordButton.setVisible(true);
        changePasswordButton.addActionListener(this);
        
        newActivityButton = new JButton(Strings.BUTTON_NEW_ACTIVITY);
        newActivityButton.setVisible(true);
        newActivityButton.addActionListener(this);

        backupButton = new JButton(Strings.BUTTON_BACKUP_DATA);
        backupButton.setVisible(true);
        backupButton.addActionListener(this);

        loadButton = new JButton(Strings.BUTTON_LOAD_DATA);
        loadButton.setVisible(true);
        loadButton.addActionListener(this);

        exitButton = new JButton(Strings.BUTTON_EXIT);
        exitButton.setVisible(true);
        exitButton.addActionListener(this);

        queryButton = new JButton(Strings.BUTTON_QUERYFRAME);
        queryButton.setVisible(true);
        queryButton.addActionListener(this);

        depManage_trigger = new JButton(Strings.BUTTON_DEP_MANAGER);
        depManage_trigger.setVisible(true);
        depManage_trigger.addActionListener(this);

        atManage_trigger = new JButton(Strings.BUTTON_AT_MANAGER);
        atManage_trigger.setVisible(true);
        atManage_trigger.addActionListener(this);

        wallPaperChooserButton = new JButton(Strings.BUTTON_CHANGE_BACKGROUND);
        wallPaperChooserButton.setVisible(true);
        wallPaperChooserButton.addActionListener(this);

        menu = new JPanel();
        menu.setSize(180, 310);
        menu.setLocation(windowX - menu.getSize().width - 50, 50);
        menu.setBorder(new BevelBorder(BevelBorder.RAISED, Color.black, Color.black));
        FlowLayout menuLayout = new FlowLayout();
        menuLayout.setVgap(10);
        menuLayout.setHgap(30);
        menu.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        menu.setLayout(menuLayout);
        menu.setVisible(true);
        menu.setOpaque(true);
        add(menu, 0);

        menu.add(exitButton, 0);
        menu.add(changePasswordButton, 0);
        menu.add(backupButton, 0);
        menu.add(loadButton, 0);
        menu.add(wallPaperChooserButton, 0);
        menu.add(atManage_trigger, 0);
        menu.add(depManage_trigger, 0);
        menu.add(queryButton, 0);
        menu.add(newActivityButton, 0);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e);
        if(e.getSource().equals(changePasswordButton)){
            this.setVisible(false);
            SetPassword sp = new SetPassword(this);
            sp.setTitle(Strings.BUTTON_CHANGE_PASSWORD);
        } else if (e.getSource().equals(exitButton)) {
            Main.terminateWamp();
            System.exit(0);
        } else if (e.getSource().equals(queryButton)) {
            this.setVisible(false);
            queryFrame.setVisible(true);
        } else if (e.getSource().equals(atManage_trigger)) {
            this.setVisible(false);
            atManager.setVisible(true);
        } else if (e.getSource().equals(depManage_trigger)) {
            this.setVisible(false);
            depManager.setVisible(true);
        } else if (e.getSource().equals(newActivityButton)) {
            this.setVisible(false);
            newActivityTask.setVisible(true);
        } else if (e.getSource().equals(wallPaperChooserButton)) {
            JFileChooser bg = new JFileChooser();
            bg.showOpenDialog(this);
            bg.setMultiSelectionEnabled(false);
            File imageFile = bg.getSelectedFile();
            if (imageFile == null) {
                Main.errorMessage(Strings.MESSAGE_WRONG_FILE, this);
                return;
            }
            if (imageFile.getName().endsWith(".jpg")
                    || imageFile.getName().endsWith(".JPG")
                    || imageFile.getName().endsWith(".png")
                    || imageFile.getName().endsWith(".PNG")
                    || imageFile.getName().endsWith(".jpeg")
                    || imageFile.getName().endsWith(".JPEG")) {
                // load the wallpaper.
                if (!DB.setBackGroundPath(imageFile.getAbsolutePath())) {
                    Main.errorMessage(Strings.MESSAGE_DB_CONNECTION_ERROR,
                            this);
                    return;
                }
                ImageIcon newImage = new ImageIcon(imageFile.getAbsolutePath());
                setWallPaper(newImage);
            } else {
                Main.errorMessage(Strings.MESSAGE_WRONG_FILE, this);
            }
        } else if (e.getSource().equals(backupButton)) {
            SafeFileSaver chooser = new SafeFileSaver();
            chooser.setMultiSelectionEnabled(false);
            chooser.showSaveDialog(this);
            File f = chooser.getSelectedFile();
            if (f == null && f.getName().endsWith(".sql")) {
                Main.errorMessage(Strings.MESSAGE_WRONG_FILE, this);
                return;
            }
            String filePath = f.getAbsolutePath();
            if (!filePath.endsWith(".sql")) {
                filePath += ".sql";
            }
            AbstractFrame.DB.exportDatabase(filePath);
        } else if (e.getSource().equals(loadButton)) {
            JFileChooser browse = new JFileChooser();
            browse.setMultiSelectionEnabled(false);
            browse.showOpenDialog(this);
            File f = browse.getSelectedFile();
            try {
                AbstractFrame.DB.importDatabase(f.getAbsolutePath());
            } catch (Exception ex) {
                Main.errorMessage(Strings.MESSAGE_WRONG_FILE, this);
            }
        }
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
        atManager.dispose();
        queryFrame.dispose();
        newActivityTask.dispose();
        depManager.dispose();
        Main.terminateWamp();
        System.gc();
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }
}