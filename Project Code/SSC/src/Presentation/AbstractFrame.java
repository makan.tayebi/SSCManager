/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation;

import Constants.Strings;
import db_code.MySQLConnector;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import ssc.Main;

/**
 *
 * @author Makan Tayebi
 */
public class AbstractFrame extends JFrame implements WindowListener, ActionListener {

    protected static MySQLConnector DB;
    protected JLabel wallPaper;
    protected AbstractFrame caller;
    protected JLabel logo;
    protected static int windowX;
    protected static int windowY;
    protected JButton backButton;

    public AbstractFrame() {
        GraphicsConfiguration config = getGraphicsConfiguration();
        final int left = Toolkit.getDefaultToolkit().getScreenInsets(config).left;
        final int right = Toolkit.getDefaultToolkit().getScreenInsets(config).right;
        final int top = Toolkit.getDefaultToolkit().getScreenInsets(config).top;
        final int bottom = Toolkit.getDefaultToolkit().getScreenInsets(config).bottom;

        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        windowX = screenSize.width - left - right;
        windowY = screenSize.height - top - bottom;
        setSize(windowX, windowY);

        logo = new JLabel(new ImageIcon(Strings.logoPath));
        logo.setSize(logo.getPreferredSize());
        logo.setLocation(0, 0);
        logo.setVisible(true);
        this.setTitle(Strings.TITLE_ABSTRACT_FRAME);
        this.setLocation(0, 5);
        this.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException ex) {
        } catch (InstantiationException ex) {
        } catch (IllegalAccessException ex) {
        } catch (UnsupportedLookAndFeelException ex) {
        }
        this.addWindowListener(this);
        this.getContentPane().setBackground(new Color(170, 170, 230));
        this.setAlwaysOnTop(false);

        setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.setResizable(false);
        this.setLayout(null);
        this.setVisible(false);
        wallPaper = new JLabel();
        wallPaper.setLocation(0, 0);
        this.add(wallPaper, -1);
        wallPaper.setVisible(true);
        backButton = new JButton(Strings.BUTTON_BACK);
        backButton.setSize(backButton.getPreferredSize());
        backButton.setLocation(5, windowY - 70);
        this.add(backButton);
        backButton.addActionListener(this);
        backButton.setVisible(true);
        this.add(logo, 0);
    }

    public void setBackground() {
        try {
            String path = DB.getBackGroundPath();
            if (path == null) {
                throw new Exception();
            }
            setWallPaper(new ImageIcon(path));
        } catch (Exception ex) {
            setWallPaper(new ImageIcon("background3.jpg"));
        }
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    public AbstractFrame getCaller() {
        return caller;
    }

    public void setCaller(AbstractFrame caller) {
        this.caller = caller;
    }

    @Override
    public void setVisible(boolean b) {
        super.setVisible(b);
        try {
            if (b == false) { // exiting this window and entering a new one.
                getCaller().setLocation(getLocation());
            } else { // coming to this window
                this.setLocation(getCaller().getLocation());
            }
        } catch (Exception ex) {
        }
    }

    public void setWallPaper(ImageIcon newImage) {
        if ((double) newImage.getIconWidth() / (double) newImage.getIconHeight()
                > (double) windowX / (double) windowY) {
            // background is too wide; stretching it vertically.
            newImage.setImage(newImage.getImage().getScaledInstance(
                    (int) ((double) newImage.getIconWidth()
                    * windowY / newImage.getIconHeight()),
                    windowY, 0));
        } else {
            // background is too lengthy; stretching it horizontally.
            newImage.setImage(newImage.getImage().getScaledInstance(windowX,
                    (int) ((double) newImage.getIconHeight()
                    * windowX / newImage.getIconWidth()), 0));
        }
        wallPaper.setIcon(newImage);
        wallPaper.setSize(wallPaper.getPreferredSize());
        add(wallPaper, -1);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(backButton)) {
            this.setVisible(false);
            caller.setVisible(true);
        }
    }
}

class SafeFileSaver extends JFileChooser {

    @Override
    public void approveSelection() {
        File f = getSelectedFile();
        if (f.exists() && getDialogType() == SAVE_DIALOG) {
            int result = Main.confirmMessage(Strings.MESSAGE_FILE_ALREADY_EXISTS, this);
            switch (result) {
                case JOptionPane.YES_OPTION:
                    super.approveSelection();
                    return;
                case JOptionPane.NO_OPTION:
                    return;
                case JOptionPane.CLOSED_OPTION:
                    return;
                case JOptionPane.CANCEL_OPTION:
                    cancelSelection();
                    return;
            }
        }
        super.approveSelection();
    }
}
