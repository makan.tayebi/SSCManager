/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation;

import Constants.Strings;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.SQLException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import ssc.Main;

/**
 *
 * @author Makan Tayebi
 */
public class ATManager extends AbstractFrame implements WindowListener, ActionListener, KeyListener {

    JPanel modifyPanel, addPanel;
    JButton addButton;
    JLabel ratioLabel_add, ATNameLabel_add;
    JTextField ATratioTextField_add, ATNameTextField_add;
    JComboBox ATComboBox;
    JButton changeButton, deleteButton;
    JLabel ratioLabel_modify, chooseATLabel, ATNameLabel_modify;
    JTextField ATratioTextField_modify, ATNameTextField_modify;
    int IDs[];
    String Names[];

    private void update() {
        IDs = AbstractFrame.DB.getAT_IDs();
        Names = AbstractFrame.DB.getAT_Names();
        /*Muting comboBox*/
        ATComboBox.removeActionListener(this);
        /*removing it's items*/
        ATComboBox.removeAllItems();
        /*adding new items*/
        for (int i = 0; i < Names.length; i++) {
            ATComboBox.addItem(Names[i]);
        }
        ATComboBox.addActionListener(this);
    }

    private void loadselectedAT(int at_ID) {
        ATNameTextField_modify.setText(AbstractFrame.DB.getAT_Name(at_ID));
        ATratioTextField_modify.setText("" + AbstractFrame.DB.getAT_ratio(at_ID));
    }

    @Override
    public void setVisible(boolean b) {
        super.setVisible(b);
        if (b) {
            update();
            try {
                ATratioTextField_add.setText(null);
                ATratioTextField_modify.setText(null);
                ATNameTextField_modify.setText(null);
                ATNameTextField_add.setText(null);
                ATComboBox.setSelectedIndex(0);
                loadselectedAT(IDs[0]);
            } catch (Exception ex) {
            }
        }
    }

    public ATManager(AbstractFrame caller) {
        this.setCaller(caller);
        this.setTitle(this.getTitle() + " - " + Strings.TITLE_ATMANAGER);
        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        initiateModifyPanel();
        initiateAddPanel();
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
        getCaller().setVisible(true);
        this.setVisible(false);
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    @Override
    public void setWallPaper(ImageIcon newImage) {
        super.setWallPaper(newImage);
    }

    @Override
    @SuppressWarnings("static-access")
    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e);
        if (e.getSource().equals(changeButton)) {
            double ratio = -1;
            try {
                ratio = Double.parseDouble(ATratioTextField_modify.getText());
            } catch (Exception ex) { //ratiotextfield_modify has wrong entries.
                Main.errorMessage(Strings.MESSAGE_WRONG_AT_RATIO, this);
                return;
            }
            if (ATComboBox.getSelectedIndex() == -1) {
                Main.errorMessage(Strings.MESSAGE_NO_AT_SELECTED, this);
                return;
            }
            modify(ATComboBox.getSelectedIndex(), ATNameTextField_modify.getText(), ratio);
        } else if (e.getSource().equals(deleteButton)) {
            if (ATComboBox.getSelectedIndex() == -1) {
                Main.errorMessage(Strings.MESSAGE_NO_AT_SELECTED, this);
                return;
            }
            delete(IDs[ATComboBox.getSelectedIndex()]);
        } else if (e.getSource().equals(addButton)) {
            double ratio = -1;
            try {
                ratio = Double.parseDouble(ATratioTextField_add.getText());
            } catch (Exception ex) {
                Main.errorMessage(Strings.MESSAGE_WRONG_AT_RATIO, this);
                return;
            }
            add(ATNameTextField_add.getText(), ratio);
        } else if (e.getSource().equals(ATComboBox)) {
            loadselectedAT(IDs[ATComboBox.getSelectedIndex()]);
        }
    }

    private void add(String name, double ratio) {
        if (AbstractFrame.DB.addActivityType(name, ratio)) {
            Main.informMessage(Strings.MESSAGE_CHANGES_APPLIED, this);
        } else {
            Main.errorMessage(Strings.MESSAGE_UNABLE_TO_CHANGE, this);
            return;
        }
        update();
        loadselectedAT(IDs[ATComboBox.getSelectedIndex()]);
    }

    @SuppressWarnings("static-access")
    private void delete(int at_ID) {
        try {
            boolean deleteSucces;
            deleteSucces = AbstractFrame.DB.deleteActivityType(at_ID, false);

            if (deleteSucces) {
                update();
                Main.informMessage(Strings.MESSAGE_CHANGES_APPLIED, this);
                update();
                try {
                    ATComboBox.setSelectedIndex(0);
                    loadselectedAT(IDs[ATComboBox.getSelectedIndex()]);
                } catch (Exception ex) {
                }
                return;
            }
            int desicion = Main.confirmMessage(Strings.MESSAGE_CONFIRM_DELETE,
                    this);
            if (desicion == 0) {
                AbstractFrame.DB.deleteActivityType(at_ID, true);
                update();
                Main.errorMessage(Strings.MESSAGE_CHANGES_APPLIED, this);
                try {
                    ATComboBox.setSelectedIndex(0);
                    loadselectedAT(IDs[ATComboBox.getSelectedIndex()]);
                } catch (Exception ex) {
                }
            }
            return;
        } catch (SQLException ex) {
            ex.printStackTrace();
            Main.errorMessage(Strings.MESSAGE_DB_CONNECTION_ERROR, this);
            return;
        }
    }

    private void modify(int selected_Index, String Name, double ratio) {
        if (AbstractFrame.DB.setAT_Name(IDs[selected_Index], Name)
                && AbstractFrame.DB.setAT_ratio(IDs[selected_Index], ratio)) {
            Main.informMessage(Strings.MESSAGE_CHANGES_APPLIED, this);
        } else {
            Main.errorMessage(Strings.MESSAGE_UNABLE_TO_CHANGE, this);
        }
        update();
        ATComboBox.setSelectedIndex(selected_Index);
        loadselectedAT(IDs[selected_Index]);
    }

    private void initiateAddPanel() {
        addPanel = new JPanel();
        addPanel.setSize(500, 85);
        this.add(addPanel, 0);
        addPanel.setLocation(windowX - 550, 260);
        addPanel.setVisible(true);
        addPanel.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
//        FlowLayout f = new FlowLayout();
//        f.setHgap(5);
        addPanel.setLayout(null);

        addPanel.setBorder(new BevelBorder(BevelBorder.RAISED, Color.black, Color.black));

        addButton = new JButton(Strings.BUTTON_ADD);
        addButton.setSize(addButton.getPreferredSize());
        addButton.setLocation(30, 50);
        
        ATNameTextField_add = new JTextField();
        ATNameTextField_add.setSize(new Dimension(400, 23));
        ATNameTextField_add.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        ATNameTextField_add.setLocation(30, 17);
        
        ATNameLabel_add = new JLabel(Strings.LABEL_AT);
        ATNameLabel_add.setLocation(435,20);
        ATNameLabel_add.setSize(ATNameLabel_add.getPreferredSize());

        ratioLabel_add = new JLabel(Strings.LABEL_RATIO);
        ratioLabel_add.setLocation(450, 50);
        ratioLabel_add.setSize(ratioLabel_add.getPreferredSize());

        ATratioTextField_add = new JTextField();
        ATratioTextField_add.setSize(new Dimension(50, 25));
        ATratioTextField_add.setMargin(new Insets(2, 2, 2, 2));
//        ATratioTextField_add.setBorder(new BevelBorder(BevelBorder.RAISED, Color.black, Color.black));
        ATratioTextField_add.addKeyListener(this);
        ATratioTextField_add.setLocation(380, 50);

        addPanel.add(ATNameLabel_add);
        addPanel.add(ATNameTextField_add);
        addPanel.add(ratioLabel_add);
        addPanel.add(ATratioTextField_add);
        addPanel.add(addButton);
        addButton.addActionListener(this);
    }

    private void initiateModifyPanel() {
        modifyPanel = new JPanel();
        modifyPanel.setSize(500, 200);
        this.add(modifyPanel, 0);
        modifyPanel.setLocation(windowX - 550, 50);
        modifyPanel.setVisible(true);
        modifyPanel.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        modifyPanel.setLayout(null);
        modifyPanel.setBorder(new BevelBorder(BevelBorder.RAISED, Color.black, Color.black));

        ratioLabel_modify = new JLabel(Strings.LABEL_RATIO);
        ratioLabel_modify.setSize(ratioLabel_modify.getPreferredSize());
        ratioLabel_modify.setLocation(450, 65);
        ratioLabel_modify.setVisible(true);

        ATratioTextField_modify = new JTextField();
        ATratioTextField_modify.setLocation(390, 60);
        ATratioTextField_modify.setVisible(true);
        ATratioTextField_modify.setSize(40, 25);

        ATComboBox = new JComboBox();
        ATComboBox.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        ATComboBox.setLocation(30, 13);
        ATComboBox.setSize(400, 20);
        ATComboBox.setVisible(true);

        chooseATLabel = new JLabel(Strings.LABEL_AT);
        chooseATLabel.setSize(chooseATLabel.getPreferredSize());
        chooseATLabel.setLocation(440, 13);
        chooseATLabel.setVisible(true);

        changeButton = new JButton(Strings.BUTTON_CHANGE);
        changeButton.setLocation(30, 120);
        changeButton.setSize(changeButton.getPreferredSize());
        changeButton.setVisible(true);

        deleteButton = new JButton(Strings.BUTTON_DELETE);
        deleteButton.setLocation(105, 120);
        deleteButton.setSize(deleteButton.getPreferredSize());
        deleteButton.setVisible(true);

        ATNameLabel_modify = new JLabel(Strings.LABEL_NAME);
        ATNameLabel_modify.setSize(ATNameLabel_modify.getPreferredSize());
        ATNameLabel_modify.setLocation(450, 93);
        ATNameLabel_modify.setVisible(true);

        ATNameTextField_modify = new JTextField();
        ATNameTextField_modify.setLocation(30, 90);
        ATNameTextField_modify.setVisible(true);
        ATNameTextField_modify.setSize(400, 25);
        ATNameTextField_modify.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);

        modifyPanel.add(ratioLabel_modify);
        modifyPanel.add(ATratioTextField_modify);
        modifyPanel.add(ATComboBox);
        modifyPanel.add(chooseATLabel);
        modifyPanel.add(changeButton);
        modifyPanel.add(deleteButton);
        modifyPanel.add(ATNameLabel_modify);
        modifyPanel.add(ATNameTextField_modify);

        ATratioTextField_modify.addKeyListener(this);
        ATComboBox.addActionListener(this);
        changeButton.addActionListener(this);
        deleteButton.addActionListener(this);
    }

    public static void main(String args[]) {
        ATManager atm = new ATManager(null);
        atm.setVisible(true);
    }
    @Override
    public void keyTyped(KeyEvent e) {
        if (e.getSource().equals(ATratioTextField_add)
                || e.getSource().equals(ATratioTextField_modify)) {
            if ((e.getKeyChar() > '9' || e.getKeyChar() < '0') && e.getKeyChar() != '.') {
                e.consume();
            }
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }
}
