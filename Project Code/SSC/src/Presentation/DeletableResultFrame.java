/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation;

import Constants.Strings;
import DataStructure.Activity;
import DataStructure.Date;
import DataStructure.Query;
import DataStructure.TableContainer;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.print.PrinterException;
import java.io.File;
import java.text.MessageFormat;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import ssc.Main;

/**
 *
 * @author Makan Tayebi
 */
public class DeletableResultFrame extends AbstractFrame implements WindowListener, ActionListener, KeyListener {

    private JLabel dateLabel, pointLabel, activityTypeLabel, departmentLabel, subjectLabel;
    private JComboBox activityTypeComboBox;
    private JComboBox departmentComboBox;
    private JTextField dateTextField[], pointTextField, subjectTextField;
    private int dep_IDs[], AT_IDs[];
    private String dep_Names[], AT_Names[];
    private Query q;
    private JTable table;
    private JScrollPane sp;
    private JPanel resultPanel, controlPanel;
    private JTextField acNumberTextField;
    private JLabel acNumberJLabel;
    private JButton printButton, deleteButton, editButton, loadButton;
    private JButton exportButton;
    private int loadedActivityID;

    private void initiateControlPanel() {
        controlPanel = new JPanel(null);
        controlPanel.setBorder(new BevelBorder(BevelBorder.RAISED, Color.black, Color.black));
        controlPanel.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        controlPanel.setLocation(240, windowY - 200);
        controlPanel.setSize(windowX - 250, 160);
        controlPanel.setVisible(true);
        add(controlPanel, 0);

        subjectLabel = new JLabel(Strings.LABEL_NAME);
        subjectLabel.setSize(subjectLabel.getPreferredSize());
        subjectLabel.setLocation(controlPanel.getWidth() - 35, 130);
        controlPanel.add(subjectLabel);
        subjectLabel.setVisible(true);

        subjectTextField = new JTextField();
        subjectTextField.setSize(200, 24);
        subjectTextField.setLocation(controlPanel.getWidth() - 250, 127);
        subjectTextField.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        controlPanel.add(subjectTextField, 0);
        subjectTextField.setVisible(true);

        acNumberJLabel = new JLabel(Strings.AC_ID);
        acNumberJLabel.setSize(acNumberJLabel.getPreferredSize());
        acNumberJLabel.setLocation(controlPanel.getWidth() - 90, 10);
        acNumberJLabel.setVisible(true);
        controlPanel.add(acNumberJLabel, 0);

        acNumberTextField = new JTextField();
        acNumberTextField.setLocation(controlPanel.getWidth() - 200, 10);
        acNumberTextField.setSize(100, 25);
        controlPanel.add(acNumberTextField, 0);
        acNumberTextField.setVisible(true);
        acNumberTextField.addKeyListener(this);

        dep_IDs = AbstractFrame.DB.getdep_IDs();
        dep_Names = AbstractFrame.DB.getdep_Names();
        AT_IDs = AbstractFrame.DB.getAT_IDs();
        AT_Names = AbstractFrame.DB.getAT_Names();

        activityTypeLabel = new JLabel(Strings.LABEL_AT);
        activityTypeLabel.setSize(activityTypeLabel.getPreferredSize());
        activityTypeLabel.setLocation(controlPanel.getWidth() - 230, 80);
        activityTypeLabel.setVisible(true);
        activityTypeLabel.setOpaque(false);
        controlPanel.add(activityTypeLabel);
        dateTextField = new JTextField[3];
        for (int i = 0; i < 3; i++) {
            dateTextField[i] = new JTextField();
            dateTextField[i].setVisible(true);
            dateTextField[i].addKeyListener(this); // 0 : year. 1:month. 2: day
            controlPanel.add(dateTextField[i], 0);
        }
        dateTextField[0].setLocation(controlPanel.getWidth() - 130, 47);
        dateTextField[1].setLocation(controlPanel.getWidth() - 85, 47);
        dateTextField[2].setLocation(controlPanel.getWidth() - 60, 47);
        dateTextField[0].setSize(40, 25);
        dateTextField[1].setSize(20, 25);
        dateTextField[2].setSize(20, 25);

        dateLabel = new JLabel(Strings.LABEL_DATE);
        dateLabel.setSize(dateLabel.getPreferredSize());
        dateLabel.setLocation(controlPanel.getWidth() - 40, 50);
        dateLabel.setVisible(true);
        controlPanel.add(dateLabel, 0);

        departmentLabel = new JLabel(Strings.LABEL_DEP);
        departmentLabel.setSize(departmentLabel.getPreferredSize());
        departmentLabel.setLocation(controlPanel.getWidth() - 230, 50);
        departmentLabel.setVisible(true);
        controlPanel.add(departmentLabel, 0);

        departmentComboBox = new JComboBox();
        DefaultListCellRenderer alignmenter = new DefaultListCellRenderer();
        alignmenter.setHorizontalAlignment(DefaultListCellRenderer.RIGHT);
        departmentComboBox.setRenderer(alignmenter);
        departmentComboBox.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        departmentComboBox.setLocation(controlPanel.getWidth() - 500, 47);
        departmentComboBox.setSize(250, 25);
        departmentComboBox.setVisible(true);
        controlPanel.add(departmentComboBox, 0);

        activityTypeComboBox = new JComboBox();
        activityTypeComboBox.setLocation(controlPanel.getWidth() - 500, 77);
        activityTypeComboBox.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        activityTypeComboBox.setSize(250, 25);
        activityTypeComboBox.setVisible(true);
        activityTypeComboBox.setRenderer(alignmenter);
        controlPanel.add(activityTypeComboBox);

        pointLabel = new JLabel(Strings.LABEL_POINT);
        pointLabel.setSize(pointLabel.getPreferredSize());
        pointLabel.setLocation(controlPanel.getWidth() - 40, 85);
        pointLabel.setVisible(true);
        controlPanel.add(pointLabel);

        pointTextField = new JTextField();
        pointTextField.setSize(70, 25);
        pointTextField.setLocation(controlPanel.getWidth() - 120, 77);
        pointTextField.setVisible(true);
        controlPanel.add(pointTextField);
        pointTextField.addKeyListener(this);

        editButton = new JButton(Strings.BUTTON_CHANGE);
        editButton.setSize(editButton.getPreferredSize());
        editButton.setLocation(130, 130);
        editButton.setVisible(true);
        controlPanel.add(editButton);
        editButton.addActionListener(this);

        deleteButton = new JButton(Strings.BUTTON_DELETE_ACTIVITY);
        deleteButton.setLocation(10, 130);
        deleteButton.setSize(deleteButton.getPreferredSize());
        deleteButton.addActionListener(this);
        deleteButton.setVisible(true);
        controlPanel.add(deleteButton, 0);

        loadButton = new JButton(Strings.BUTTON_LOAD);
        loadButton.setSize(loadButton.getPreferredSize());
        loadButton.setLocation(controlPanel.getWidth() - 300, 10);
        loadButton.setVisible(true);
        controlPanel.add(loadButton);
        loadButton.addActionListener(this);
    }

    @Override
    public void setVisible(boolean b) {
        super.setVisible(b);
        if (b) {
            updateControlPanel();
        }
    }

    public DeletableResultFrame(AbstractFrame caller) {
        loadedActivityID = -1;
        setCaller(caller);
        this.setTitle(this.getTitle() + " - " + Strings.TITLE_DETAILEDRESULTFRAME);
        resultPanel = new JPanel(null);
        resultPanel.setBorder(new BevelBorder(BevelBorder.RAISED, Color.black, Color.black));
        sp = new JScrollPane(resultPanel);
        sp.setLocation(240, 10);
        sp.setSize(windowX - 250, windowY - 220);
        this.add(sp, 0);
        sp.setVisible(true);
        resultPanel.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);


        printButton = new JButton(Strings.BUTTON_PRINT);
        printButton.setLocation(10, windowY - 250);
        printButton.setSize(printButton.getPreferredSize());
        printButton.addActionListener(this);
        printButton.setVisible(true);
        sp.add(printButton, 0);

        exportButton = new JButton(Strings.BUTTON_EXPORT);
        exportButton.setSize(exportButton.getPreferredSize());
        exportButton.setLocation(10, windowY - 280);
        exportButton.addActionListener(this);
        exportButton.setVisible(true);
        sp.add(exportButton, 0);

        initiateControlPanel();
//        updateControlPanel();

        table = null;
        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    }

    private void updateControlPanel() {
        dep_IDs = AbstractFrame.DB.getdep_IDs();
        dep_Names = AbstractFrame.DB.getdep_Names();
        AT_IDs = AbstractFrame.DB.getAT_IDs();
        AT_Names = AbstractFrame.DB.getAT_Names();

        departmentComboBox.removeAllItems();
        activityTypeComboBox.removeAllItems();

        for (int i = 0; i < dep_Names.length; i++) {
            departmentComboBox.addItem(dep_Names[i]);
        }
        for (int i = 0; i < AT_Names.length; i++) {
            activityTypeComboBox.addItem(AT_Names[i]);
        }
    }

    private void showData(TableContainer p) {
        showData(p, q);
    }

    public void showData(TableContainer p, Query q) {
        this.q = q;
        resultPanel.removeAll();
        resultPanel.add(p);
        p.setLocation(sp.getWidth() / 2 - p.getWidth() / 2, 100);
        table = p.getTable();
        table.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        getCaller().setVisible(false);
        setVisible(true);
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
        getCaller().setVisible(true);
        this.setVisible(false);
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    private void deleteActivity() {
        try {
            if (AbstractFrame.DB.deleteActivity(loadedActivityID)) {
                Main.informMessage(Strings.MESSAGE_CHANGES_APPLIED, this);
                showData(((QueryFrame) getCaller()).detailedTable(AbstractFrame.DB.giveDetailedAnswer(q), q));
            } else {
                Main.informMessage(Strings.MESSAGE_UNABLE_TO_CHANGE, this);
            }
        } catch (Exception ex) {
            Main.errorMessage(Strings.MESSAGE_DB_CONNECTION_ERROR, this);
        }
        fillFields(null);
    }

    private void loadActivity() {
        Activity act = null;
        try {
            act = AbstractFrame.DB.getActivity(Integer.parseInt(
                    acNumberTextField.getText()));
            loadedActivityID = act.getID();
        } catch (Exception ex) {
            Main.errorMessage(Strings.MESSAGE_NO_SUCH_AC_ID, this);
            loadedActivityID = -1;
        }
        fillFields(act);
    }

    private void updateActivity() {
        if (loadedActivityID == -1) {
            Main.errorMessage(Strings.MESSAGE_NO_AC_SELECTED, this);
            fillFields(null);
        } else {
            Activity act = new Activity();
            try {
                act.setDate(new Date(
                        Integer.parseInt(dateTextField[0].getText()),
                        Integer.parseInt(dateTextField[1].getText()),
                        Integer.parseInt(dateTextField[2].getText())));
            } catch (Exception ex) {
                Main.errorMessage(Strings.MESSAGE_WRONG_DATE, this);
                return;
            }
            act.setID(loadedActivityID);
            act.setDepartmentID(dep_IDs[departmentComboBox.getSelectedIndex()]);
            act.setTypeID(AT_IDs[activityTypeComboBox.getSelectedIndex()]);
            act.setPoint(Integer.parseInt(pointTextField.getText()));
            act.setSubject(subjectTextField.getText());
            AbstractFrame.DB.updateActivity(act);
            showData(((QueryFrame) getCaller()).detailedTable(
                    AbstractFrame.DB.giveDetailedAnswer(q), q));
            Main.informMessage(Strings.MESSAGE_CHANGES_APPLIED, this);
        }
    }

    private void fillFields(Activity act) {
        if (act == null) {
            subjectTextField.setText(null);
            pointTextField.setText(null);
            dateTextField[0].setText(null);
            dateTextField[1].setText(null);
            dateTextField[2].setText(null);
            acNumberTextField.setText(null);
            departmentComboBox.setSelectedIndex(0);
            activityTypeComboBox.setSelectedIndex(0);
            return;
        }
        subjectTextField.setText(act.getSubject());
        pointTextField.setText("" + act.getPoint());
        dateTextField[0].setText("" + act.getDate().getYear());
        dateTextField[1].setText("" + act.getDate().getMonth());
        dateTextField[2].setText("" + act.getDate().getDay());
        for (int i = 0; i < dep_IDs.length; i++) {
            if (dep_IDs[i] == act.getDepartmentID()) {
                departmentComboBox.setSelectedIndex(i);
                break;
            }
        }
        for (int i = 0; i < AT_IDs.length; i++) {
            if (AT_IDs[i] == act.getTypeID()) {
                activityTypeComboBox.setSelectedIndex(i);
                break;
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e);
        if (e.getSource().equals(printButton)) {
            print();
        } else if (e.getSource().equals(deleteButton)) {
            int answer = Main.confirmMessage(Strings.MESSAGE_CONFIRM_DELETE_ACTIVITY, this);
            if(answer == 0){
                deleteActivity();
            }
        } else if (e.getSource().equals(editButton)) {
            updateActivity();
        } else if (e.getSource().equals(loadButton)) {
            loadActivity();
        } else if (e.getSource().equals(exportButton)) {
            // choose export files.
            JFileChooser chooser = new JFileChooser(
                    System.getProperty("user.home")
                    + File.pathSeparator + "Desktop");
            chooser.showSaveDialog(this);
            chooser.setMultiSelectionEnabled(false);
            if (chooser.getSelectedFile() == null) {
                Main.errorMessage(Strings.MESSAGE_WRONG_FILE, this);
                return;
            }
            String dest = chooser.getSelectedFile().getAbsolutePath();
            if (!dest.endsWith(".xls")) {
                dest += ".xls";
            }
            OverallResultFrame.export(table, dest, Strings.DETAIL, this);
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        if ( (e.getSource().equals(acNumberTextField)
                || e.getSource().equals(dateTextField[0])
                || e.getSource().equals(dateTextField[1])
                || e.getSource().equals(dateTextField[2]) )
                && (e.getKeyChar() > '9'
                || e.getKeyChar() < '0')) {
            e.consume();
            return;
        }
        if(e.getSource().equals(dateTextField[2]) && dateTextField[2].getText().length() == 1){
            dateTextField[1].requestFocus();
        } else if(e.getSource().equals(dateTextField[1]) && dateTextField[1].getText().length() == 1){
            dateTextField[0].requestFocus();
        } else if(e.getSource().equals(dateTextField[0]) && dateTextField[0].getText().length() == 3){
            pointTextField.requestFocus();
        }
    }

    private void print() {
        try {
            MessageFormat headerFormat = new MessageFormat("Page {0}");
            MessageFormat footerFormat = new MessageFormat("- {0} -");
            table.print(JTable.PrintMode.FIT_WIDTH, headerFormat, footerFormat);
        } catch (PrinterException pe) {
            System.err.println("Error printing: " + pe.getMessage());
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }
}
