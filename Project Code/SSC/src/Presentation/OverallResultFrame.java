/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation;

import Constants.Strings;
import DataStructure.TableContainer;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.print.PrinterException;
import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.BevelBorder;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.Orientation;
import jxl.format.VerticalAlignment;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import ssc.Main;

/**
 *
 * @author Makan Tayebi
 */
public class OverallResultFrame extends AbstractFrame implements WindowListener, ActionListener {

    private JTable table;
    private JPanel mainPanel;
    private JButton printButton, exportButton;

    public OverallResultFrame(AbstractFrame caller) {
        this.setTitle(this.getTitle() + " - " + Strings.TITLE_OVERALLRESULTFRAME);
        setCaller(caller);
//        setWallPaper(new ImageIcon(AbstractFrame.DB.getBackGroundPath()));
        initiateButtons();
        table = null;
    }

    private void initiateButtons() {
        printButton = new JButton(Strings.BUTTON_PRINT);
        printButton.setSize(printButton.getPreferredSize());
        printButton.setLocation(10, 10);
        printButton.addActionListener(this);
        printButton.setVisible(true);

        exportButton = new JButton(Strings.BUTTON_EXPORT);
        exportButton.setSize(exportButton.getPreferredSize());
        exportButton.setLocation(100, 10);
        exportButton.addActionListener(this);
        exportButton.setVisible(true);
    }

    public void showData(TableContainer panel) {
        if (mainPanel != null) {
            mainPanel.setVisible(false);
        }
        mainPanel = new JPanel(null);
        mainPanel.setSize(windowX - 340, windowY - 60);
        mainPanel.setBorder(new BevelBorder(BevelBorder.RAISED, Color.black, Color.black));
        mainPanel.setLocation((windowX - mainPanel.getWidth()) / 2 + 20, 20);
        mainPanel.add(printButton, 0);
        mainPanel.add(exportButton);
        this.add(mainPanel, 0);
        mainPanel.add(panel);
        this.setLayout(null);
        mainPanel.setVisible(true);
        table = panel.getTable();
        panel.setLocation(mainPanel.getWidth() / 2 - panel.getWidth() / 2, 30);
        getCaller().setVisible(false);
        setVisible(true);
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
        getCaller().setVisible(true);
        this.setVisible(false);
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    private void print() {
        try {
            MessageFormat headerFormat = new MessageFormat("Page {0}");
            MessageFormat footerFormat = new MessageFormat("- {0} -");
            table.print(JTable.PrintMode.FIT_WIDTH, headerFormat, footerFormat);
        } catch (PrinterException pe) {
            System.err.println("Error printing: " + pe.getMessage());
            pe.printStackTrace();
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e);
        if (e.getSource().equals(printButton)) {
            print();
        } else if (e.getSource().equals(exportButton)) {
            // choose export files.
            JFileChooser chooser = new JFileChooser(
                    System.getProperty("user.home")
                    + File.pathSeparator + "Desktop");
            chooser.showSaveDialog(this);
            chooser.setMultiSelectionEnabled(false);
            if (chooser.getSelectedFile() == null) {
                Main.errorMessage(Strings.MESSAGE_WRONG_FILE, this);
                return;
            }
            String dest = chooser.getSelectedFile().getAbsolutePath();
            if (!dest.endsWith(".xls")) {
                dest += ".xls";
            }
            export(table, dest, Strings.OVERALL_LABEL, this);
        }
    }
    
    protected static void export(JTable table, String filePath, String subject,
            Component caller) {
        WritableWorkbook wwb = null;
        File f = new File(filePath);
        try {
            wwb = Workbook.createWorkbook(f);
        } catch (IOException ex) {
            Main.errorMessage(Strings.MESSAGE_WRONG_FILE, caller);
            ex.printStackTrace();
            return;
        }
        WritableFont regFont = new WritableFont(WritableFont.createFont(
                Strings.contentFontName), 10);
        WritableFont boldFont = new WritableFont(WritableFont.createFont(
                Strings.contentFontName), 10, WritableFont.BOLD);
        WritableSheet sheet = wwb.createSheet(subject, 1);
        try {
            WritableCellFormat regularFormat = new WritableCellFormat();
            regularFormat.setFont(regFont);
            regularFormat.setAlignment(Alignment.CENTRE);
            regularFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            regularFormat.setBackground(Colour.AQUA);
            regularFormat.setWrap(true);

            WritableCellFormat headerFormat = new WritableCellFormat();
            headerFormat.setBackground(Colour.YELLOW2);
            headerFormat.setAlignment(Alignment.CENTRE);
            headerFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            headerFormat.setOrientation(Orientation.HORIZONTAL);
            headerFormat.setFont(boldFont);
            headerFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
            headerFormat.setWrap(true);

            WritableCellFormat oddTableLineFormat = new WritableCellFormat();
            WritableCellFormat evenTableLineFormat = new WritableCellFormat();
            oddTableLineFormat.setBackground(Colour.VERY_LIGHT_YELLOW);
            evenTableLineFormat.setFont(regFont);
            evenTableLineFormat.setAlignment(Alignment.CENTRE);
            evenTableLineFormat.setWrap(true);
            oddTableLineFormat.setFont(regFont);
            oddTableLineFormat.setAlignment(Alignment.CENTRE);
            oddTableLineFormat.setWrap(true);

            WritableCellFormat titleFormat = new WritableCellFormat();
            titleFormat.setBackground(Colour.BLUE);
            titleFormat.setFont(boldFont);
            titleFormat.setWrap(true);

            jxl.write.Label l = new jxl.write.Label(0, 0, subject, titleFormat);
            sheet.addCell(l);
            try {
                OverallResultFrame orf = (OverallResultFrame) caller;
                l = new jxl.write.Label(0, 1, Strings.POINTS_SLASH_NUMBERS, regularFormat);
                sheet.addCell(l);
            } catch (Exception ex) {
                /**
                 * this means that caller is not overall Result Frame;
                 */
            }
            for (int i = 0; i < table.getColumnCount(); i++) {
                l = new jxl.write.Label(i + 1, 0,
                        table.getColumnModel().getColumn(i).getHeaderValue().toString(), headerFormat);
                sheet.addCell(l);
            }
            jxl.write.Number numberCell = null;
            for (int col = 0; col < table.getColumnCount(); col++) {
                for (int row = 0; row < table.getRowCount(); row++) {
                    try {
                        numberCell = new jxl.write.Number(col + 1,
                                row + 1, Integer.parseInt(
                                table.getModel().getValueAt(row, col).toString()));
                    } catch (Exception exInt) {
                        try {
                            numberCell = new jxl.write.Number(col + 1,
                                    row + 1, Double.parseDouble(
                                    table.getModel().getValueAt(row, col).toString()));
                        } catch (Exception exDouble) {
                            l = new jxl.write.Label(col + 1, row + 1,
                                    table.getModel().getValueAt(row, col).toString());
                        }
                    }
                    try {
                        if (row % 2 == 0) {
                            numberCell.setCellFormat(evenTableLineFormat);
                        } else {
                            numberCell.setCellFormat(oddTableLineFormat);
                        }
                        sheet.addCell(numberCell); // cell is double or int.
                    } catch (NullPointerException ex) { // cell is simply a string.
                        if (row % 2 == 0) {
                            l.setCellFormat(evenTableLineFormat);
                        } else {
                            l.setCellFormat(oddTableLineFormat);
                        }
                        sheet.addCell(l);
                    }
                    numberCell = null; // set numberCell to get proper exception next round if next value is not number.
                }
            }
//            sheet.getSettings().setFitWidth(10);
            sheet.getSettings().setDefaultColumnWidth(20);
//            sheet.getSettings().setDefaultRowHeight(2);
//            sheet.getSettings().setLeftMargin(2);
//            sheet.getSettings().setRightMargin(2);
//            sheet.setColumnView(1, new CellView())
            wwb.write();
            wwb.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            return;
        }
        Main.informMessage(Strings.OPERATION_SUCCESSFUL, caller);
    }
}
