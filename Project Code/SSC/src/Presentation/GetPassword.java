/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation;

import Constants.Strings;
import DataStructure.Restricted;
import db_code.MySQLConnector;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import ssc.Main;

/**
 *
 * @author Emertat
 */
public class GetPassword extends JFrame implements ActionListener, KeyListener {

    private JButton submitButton, cancelButton;
    private JPasswordField passwordField;
    private JLabel passwordLabel;
    private MySQLConnector DB;
    private int width = 250, height = 130;
    private Restricted caller;

    public GetPassword(Restricted caller) {
        this.caller = caller;
        DB = new MySQLConnector();
        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        this.setSize(width, height);
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation((screenSize.width - width) / 2, (screenSize.height - height) / 2);
        this.setLayout(null);


        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException ex) {
        } catch (InstantiationException ex) {
        } catch (IllegalAccessException ex) {
        } catch (UnsupportedLookAndFeelException ex) {
        }

        passwordLabel = new JLabel(Strings.LABEL_PASSWORD_COLON);
        passwordLabel.setSize(passwordLabel.getPreferredSize());
        passwordLabel.setLocation(170, 10);
        passwordLabel.setVisible(true);
        this.add(passwordLabel);

        passwordField = new JPasswordField();
        passwordField.setSize(150, 20);
        passwordField.setLocation(10, 10);
        passwordField.addKeyListener(this);
        passwordField.setVisible(true);
        this.add(passwordField);

        submitButton = new JButton(Strings.BUTTON_CONTINUE);
        submitButton.setSize(submitButton.getPreferredSize());
        submitButton.setLocation(10, 50);
        submitButton.addActionListener(this);
        submitButton.setVisible(true);
        this.add(submitButton);

        cancelButton = new JButton(Strings.BUTTON_CANCEL);
        cancelButton.setSize(cancelButton.getPreferredSize());
        cancelButton.setLocation(80, 50);
        cancelButton.addActionListener(this);
        cancelButton.setVisible(true);
        this.add(cancelButton);
        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(submitButton)) {
            checkLogin();
        } else if (e.getSource().equals(cancelButton)) {
            caller.cancelAction();
            this.dispose();
        }
    }

    private void checkLogin() {
        String pass = new String(passwordField.getPassword());
        if (DB.isPassValid(pass)) {
            caller.passwordSet(pass);
            this.dispose();
        } else {
            // enter password again
            Main.errorMessage(Strings.MESSAGE_WRONG_PASSWORD, this);
        }

    }

    @Override
    public void keyTyped(KeyEvent e) {
        if (e.getSource().equals(passwordField)) {
            if (e.getKeyChar() == '\n') {
                checkLogin();
            }
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }
}
